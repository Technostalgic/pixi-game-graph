///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

import { Collider } from "./collider";
import { SceneEntity } from "./sceneEntity";

/** Interface to allow a scene entity to be able to be activated or deactivated */
export interface ICollider extends SceneEntity{
	
	/** Whether or not the bject is currently active in the scene */
	get collider(): Collider;
}

export namespace ICollider{
	
	/** Type checking discriminator for IActivate */
	export function implementedIn(obj: SceneEntity): obj is ICollider{
		return(
			"collider" in obj
		);
	}
}