///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { SceneEntity } from './sceneEntity';

/**
 * base class used for visual effects
 */
export abstract class Effect extends SceneEntity{
	constructor(){ super(); }
}