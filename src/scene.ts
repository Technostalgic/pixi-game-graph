///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Game } from './game';
import { SceneEntity } from './sceneEntity';
import { ColliderPartitions } from './colliderPartitions';

/**
 * data structure that contains game entities
 */
export class Scene extends SceneEntity{

	public constructor(game: Game) {

		super();

		if(game !== undefined){
			this._parentGame = game;
		}
		
		this._parentScene = this;
		this._name = "Scene";
		
		this._colliders = new ColliderPartitions();
	}

	// ---------------------------------------------------------------------------------------------
	private _entitiesToRemove: Array<SceneEntity> = new Array<SceneEntity>();
	private _colliders: ColliderPartitions = null;
	protected _parentGame: Game = null;

	public override get parentScene() { return this; }
	public override get parentGame() { return this._parentGame; }
	
	public get entitiesToRemove(){ return this._entitiesToRemove; }
	public get colliders() { return this._colliders; }

	// ---------------------------------------------------------------------------------------------
	/**
	 * should be called when scene is switched to
	 */
	public OnEnter(): void{ }

	/**
	 * should be called when scene is switched away from
	 */
	public OnExit(): void{ }

	// ---------------------------------------------------------------------------------------------
	/**
	 * Check collisions between all the colliders in the scene
	 */
	public CheckCollisions(): void{

		let collisions = this._colliders.FindCollisions();

		for(let i = collisions.length - 1; i >= 0; i--){
			collisions[i].colliderA.onCollision.Invoke(collisions[i]);
			collisions[i].colliderB.onCollision.Invoke(collisions[i]);
		}
	}

	private RemoveFlaggedEntities(): void {

		// iterate through each entity flagged for removal and remove them all
		for(let i = this._entitiesToRemove.length - 1; i >= 0; i--){
			this._entitiesToRemove[i].destroy();
		}

		// remove them from the list
		this._entitiesToRemove.splice(0, this._entitiesToRemove.length);
	}

	/**
	 * override to implement update functionality for the scene
	 * @param deltaTime 
	 */
	public Update(deltaTime: number){ 

		// update all direct children
		super.Update(deltaTime);

		// check collisions
		this.CheckCollisions();

		// remove entities that are to be removed
		this.RemoveFlaggedEntities();
	}

	/**
	 * override to implement rendering functionality for the scene
	 */
	public Draw(renderer: PIXI.Renderer): void{

		// render the scene graph
		this.scale.set(2);
		renderer.render(this);
		this.scale.set(1);
	}

	// ---------------------------------------------------------------------------------------------
	public override Clone(): SceneEntity {
		
		let r = new (Object.getPrototypeOf(this).constructor)(this.parentGame) as Scene;
		r.CopyChildren(this);
		r.CopyFields(this);
		return r;
	}

	/** @inheritdoc */
	public override GetSerializedObject() {
		
		// we don't want to serialize colldiers or parent game reference
		let o = [this._colliders, this._parentGame];
		this._colliders = undefined;

		let r = super.GetSerializedObject();
		
		this._colliders = o[0] as ColliderPartitions;

		return r;
	}
}