///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { ITarget } from './iTarget';
import { IVect } from './math';
import { PhysicsEntity } from './physicsEntity';
import { Collider } from './collider';
import { ICollision } from './iCollision';
import { SceneEntity } from './sceneEntity';
import { ICollider } from './iCollider';

/**
 * The actor class is meant to be a boilerplate class for entities in game such as enemies, NPCs,
 * the player, or anything else that could be considered some sort of character
 */
export abstract class Actor extends PhysicsEntity implements ITarget, ICollider {

	public constructor(){
		super();
		this.CreateCollider();

		let ths = this;
		this.collider.onCollision.AddListener(function(col){
			ths.OnCollision(col);
		});
	}
	
	public static readonly Types: any[] = [];

	protected _maxHealth: number;
	protected _health: number = 0;

	public get health() { return this._health; }
	public set health(val) {
		this._health = val;
	}

	public get maxHealth() { return this._maxHealth; }

	public Hit(damage: number, impulse: IVect, damager: SceneEntity = null): void {

		this.health -= damage;
		this.ApplyImpulse(impulse);

		// kill check
		if(this._health <= 0) this.Kill(damager);
	}

	protected _collider: Collider;

	public get collider(){ return this._collider; }

	/** Called when the actor object is constructed */
	protected abstract CreateCollider(): void;

	/** Called when the actor's collider hits another collider */
	protected abstract OnCollision(collision: ICollision): void;

	/** @inheritdoc */
	public override destroy(){
		this.collider.destroy();
		super.destroy();
	}
	
	/**
	 * kills the enemy
	 * @param killer the object that killed the enemy
	 */
	public Kill(killer: SceneEntity = null): void{

		console.log(this.name + " killed by " + killer?.name);
		this.RemoveFromScene();
	}
}