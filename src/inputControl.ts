///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { Game } from "./game";

export enum InputCondition{
	whileHeld,
	onPress,
	onRelease
}

export enum InputType{
	keyboardButton,
	mouseButton
}

/**
 * A structure that tracks certain mapped inputs and their current state, this should be used for
 * input actions such as player movement, ui navigation, etc. as it provides an easy interface for
 * mapping, remapping, and state polling
 */
export class InputControl{

	public constructor(){ }

	/**
	 * Create an input control with the specified input
	 * @param type what piece of hardware the input is coming from
	 * @param code the id of the button on the hardware (ie keyCode if from keyboard)
	 * @param condition controls when the control is triggered
	 */
	public static Create(type: InputType, code: number, condition: InputCondition): InputControl{

		let r = new InputControl();
		r._type = type;
		r._inputCode = code;
		r._triggerCondition = condition;

		return r;
	}

	private _triggerCondition: InputCondition = InputCondition.whileHeld;
	private _inputCode: number = 0;
	private _type: InputType = InputType.keyboardButton;
	private _isHeld: boolean = false;
	private _heldLastTick: boolean = false;
	
	public get triggerCondition() { return this._triggerCondition; }
	public set triggerCondition(val){ this._triggerCondition = val; }

	public get inputCode() { return this._inputCode; }
	public set inputCode(val) { this._inputCode = val; }

	public get type() { return this._type; }
	public set type(val) { this._type = val; }

	// ---------------------------------------------------------------------------------------------

	/** 
	 * update the control to reflect thr current gamestate 
	 * @param game the game instance that this control is reporting input for
	 */
	public Update(game: Game): void{
		
		let held = false;
		switch(this._type){
			case InputType.keyboardButton:
				held = game.keysPressed[this._inputCode];
				break;
			case InputType.mouseButton:
				held = game.mousePressed;
				break;
		}

		this._heldLastTick = this._isHeld;
		this._isHeld = held;
	}

	/** determines whether or not the control has been triggered based on it's trigger condition */
	public IsTriggered(): boolean{

		switch(this._triggerCondition){
			case InputCondition.whileHeld:
				return this._isHeld;
			case InputCondition.onPress:
				return (!this._heldLastTick) && this._isHeld;
			case InputCondition.onRelease:
				return this._heldLastTick && (!this._isHeld);
		}

		return false;
	}

	/** returns true if the input is currently held down */
	public IsHeld(): boolean{
		return this._isHeld;
	}
}