///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';

export interface IVect extends PIXI.IPointData { }

export interface IRect {
	position: IVect;
	size: IVect;
}

export enum Side{
	none = 0,
	left = 1,
	right = 2,
	up = 4,
	top = 4,
	down = 8,
	bottom = 8,
	all = 15
}

export namespace Side{

	export function RotateCW(side: Side): Side{
		switch(side){
			case Side.left: return Side.up;
			case Side.right: return Side.down;
			case Side.up: return Side.right;
			case Side.down: return Side.left;
		}
		return Side.none;
	}
	
	export function RotateCCW(side: Side): Side{
		switch(side){
			case Side.left: return Side.down;
			case Side.right: return Side.up;
			case Side.up: return Side.left;
			case Side.down: return Side.right;
		}
		return Side.none;
	}

	export function Opposite(side: Side): Side{
		switch(side){
			case Side.left: return Side.right;
			case Side.right: return Side.left;
			case Side.up: return Side.down;
			case Side.down: return Side.up;
		}
		return Side.none;
	}

	export function FromDirection(direction: IVect): Side{

		if(direction.x == 0 && direction.y == 0)
			return Side.none;

		if(Math.abs(direction.x) > Math.abs(direction.y)){
			return direction.x < 0 ? Side.left : Side.right;
		}

		else{
			return direction.y < 0 ? Side.up : Side.down;
		}
	}

	export function GetAngle(side: Side): number{
		switch(side){
			case Side.left: return Math.PI;
			case Side.right: return 0;
			case Side.up: return - Math.PI / 2;
			case Side.down: return Math.PI / 2;
		}
	}

	export function GetVector(side: Side): IVect{
		switch(side){
			case Side.left: return Vect.Create(-1, 0);
			case Side.right: return Vect.Create(1, 0);
			case Side.up: return Vect.Create(0, -1);
			case Side.down: return Vect.Create(0, 1);
		}
		return Vect.Create(0);
	}
}

export namespace Maths{

	/**
	 * calculates the smallest signed distance between two angles in radians
	 * @param target the target angle to subtract from the source
	 * @param source the source angle to be subtracted from
	 */
	export function SignedAngleDif(target: number, source:number): number{

		let twoPi = Math.PI * 2;
		let pi = Math.PI;
	
		let r = target - source;
		r = ((r + pi) % twoPi + twoPi) % twoPi;
		r -= pi;
		return r;
	}
}

export namespace Vect{

	/**
	 * create a vector with the specified components
	 * @param x the x component of the vector
	 * @param y the y component of the vector
	 */
	export function Create(x: number = 0, y: number = x): IVect{
		return{
			x: x,
			y: y
		}
	}

	export function Clone(vect: IVect){
		return{
			x: vect.x,
			y: vect.y
		}
	}

	export function FromDirection(direction: number, magnitude: number = 1){
		return Create(Math.cos(direction) * magnitude, Math.sin(direction) * magnitude);
	}

	/**
	 * interpolate from point a to b, returns the interpolated value
	 * @param from the vector to interpolate from, vector A
	 * @param to the vector to interpolate to, vector B
	 * @param delta how much interpolation from A to B has elapsed; 0 for none, 1 for 100%
	 */
	export function Lerp(from: IVect, to: IVect, delta: number): IVect{

		let invDelta = 1 - delta;

		return {
			x: from.x * invDelta + to.x * delta,
			y: from.y * invDelta + to.y * delta
		};
	}

	/**
	 * calculate the euclidean distance between two vectors
	 * @param a the first vector
	 * @param b the second vector
	 */
	export function Distance(a: IVect, b: IVect){

		let dx = a.x - b.x;
		let dy = a.y - b.y;

		return Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * returns the distance between two vectors, squared (more performant than calculating distance)
	 * @param a the first vector
	 * @param b the second vector
	 * @returns 
	 */
	export function DistanceSquared(a: IVect, b: IVect){

		let dx = a.x - b.x;
		let dy = a.y - b.y;

		return dx * dx + dy * dy;
	}

	/**
	 * Add together two vectors
	 */
	export function Add(a: IVect, b: IVect): IVect{
		return{
			x: a.x + b.x,
			y: a.y + b.y
		}
	}

	/**
	 * subtract b from a
	 */
	export function Subtract(a: IVect, b: IVect): IVect{
		return{
			x: a.x - b.x,
			y: a.y - b.y
		}
	}

	/**
	 * multiply each component of a with each component of b
	 */
	export function Multiply(a: IVect, b: IVect): IVect{
		return {
			x: a.x * b.x,
			y: a.y * b.y
		}
	}

	/**
	 * scale multuply each component a vector by a scalar
	 */
	export function MultiplyScalar(a: IVect, scalar: number): IVect{
		return{
			x: a.x * scalar,
			y: a.y * scalar
		}
	}

	export function Magnitude(a: IVect): number{
		return Math.sqrt(a.x * a.x + a.y * a.y);
	}

	export function Equal(a: IVect, b: IVect, leniency: number = 0.001): boolean{
		return (
			Math.abs(a.x - b.x) <= leniency &&
			Math.abs(a.y - b.y) <= leniency
		);
	}

	/**
	 * Calculate the dot product between two vectors
	 * @param a the first vector
	 * @param b the second vector
	 */
	export function Dot(a: IVect, b: IVect): number{
		return a.x * b.x + a.y * b.y;
	}

	/** 
	 * calculate the projection of the specified vector onto another vector
	 * @param a the vector to project
	 * @param target the vector that a will be projected onto
	 */
	export function Project(a: IVect, target: IVect): IVect{
		
		let dot = Dot(a, target);
		let quotient = Dot(target, target);
		let factor = dot / quotient;
		let r = MultiplyScalar(target, factor);

		return r;
	}

	export function Direction(a: IVect): number{
		return Math.atan2(a.y, a.x);
	}

	export function Normalize(a: IVect): IVect{

		let mag = Magnitude(a);

		if(mag <= 0)
			return {x: 0, y: 0};

		return MultiplyScalar(a, 1/mag);
	}

	export function Rotate(a: IVect, rotation: number): IVect{

		let mag = Magnitude(a);
		let dir = Direction(a);
		dir += rotation;

		return FromDirection(dir, mag);
	}

	export function Invert(a: IVect): IVect{
		return {x: -a.x, y: -a.y};
	}
}

export namespace Rect{

	export function Create(position: IVect = Vect.Create(), size: IVect = Vect.Create()): IRect{
		return {
			position: position,
			size: size
		};
	}

	export function Clone(rect: IRect): IRect{ 
		return{
			position: Vect.Clone(rect.position),
			size: Vect.Clone(rect.size)
		}
	}

	export function Center(rect: IRect): IVect{
		return{
			x: rect.position.x + rect.size.x * 0.5,
			y: rect.position.y + rect.size.y * 0.5
		};
	}

	export function Overlaps(a: IRect, b: IRect): Boolean{
		
		return !(
			a.position.x > b.position.x + b.size.x ||
			a.position.x + a.size.x < b.position.x ||
			a.position.y > b.position.y + b.size.y ||
			a.position.y + a.size.y < b.position.y
		);
	}

	/**
	 * expands the rect a to fully overlap rect b, returns the modified rect a
	 * @param a the rect to expand (this rect is modified)
	 * @param b the rect that this rect should overlap
	 */
	export function ExpandToOverlap(a: IRect, b: IRect): IRect{

		let minX = Math.min(a.position.x, b.position.x);
		let minY = Math.min(a.position.y, b.position.y);
		let maxX = Math.max(a.position.x + a.size.x, b.position.x + b.size.x);
		let maxY = Math.max(a.position.y + b.size.y, b.position.y + b.size.y);

		if(minX < a.position.x){
			let dif = a.position.x - minX;
			a.position.x -= dif;
			a.size.x += dif;
		}
		if(minY < a.position.y){
			let dif = a.position.y - minY;
			a.position.y -= dif;
			a.size.y += dif;
		}
		if(maxX > a.position.x + a.size.x){
			a.size.x += maxX - (a.position.x + a.size.x);
		}
		if(maxY > a.position.y + a.size.y){
			a.size.y += maxY - (a.position.y + a.size.y);
		}

		return a;
	}

	/** Return a rect that overlaps each specified rect */
	export function OverlapRect(a: IRect, b: IRect): IRect{

		let minX = a.position.x + a.size.x;
		let minY = a.position.y + a.size.y;
		let maxX = a.position.x;
		let maxY = a.position.y;

		minX = Math.min(minX, b.position.x + b.size.x);
		minY = Math.min(minY, b.position.y + b.size.y);
		maxX = Math.max(maxX, b.position.x);
		maxY = Math.max(maxY, b.position.y);

		return Rect.Create(Vect.Create(minX, minY), Vect.Create(maxX - minX, maxY - minY));
	}

	/**
	 * Returns the side of the rect that is closest to the specified point
	 * @param a the rect to check the sides of
	 * @param b the point to compare the sides to
	 */
	export function ClosestSide(rect: IRect, point: IVect): Side{

		let aspect = rect.size.x / rect.size.y;
		let dif = Vect.Subtract(point, Rect.Center(rect));

		dif.x /= aspect;

		if(Math.abs(dif.x) > Math.abs(dif.y)){
			return dif.x > 0 ? Side.right : Side.left;
		}
		return dif.y > 0 ? Side.down : Side.up;
	}
}