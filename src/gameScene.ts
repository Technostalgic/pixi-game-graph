///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Game } from './game';
import { Scene } from './scene';
import { AABBCollider, CollisionLayer } from './collider';
import { Vect } from './math';

/**
 * A type of scene which is intended to be used for gameplay
 */
export class GameScene extends Scene{

	constructor(game: Game){
		super(game);
		this._name = "GameScene";
	}

	public static BasicScene(game: Game): GameScene{

		let r = new GameScene(game);

		r.GenerateBasicObjects();

		return r;
	}

	private GenerateBasicObjects(): void{

		let groundBottom = new AABBCollider();
		groundBottom.layer = CollisionLayer.terrain;
		groundBottom.halfExtents = Vect.Create(150, 10);
		groundBottom.position.set(200, 250);
		groundBottom.CreateWireframe(0xFFFFFF, 1);
		
		let groundLeft = new AABBCollider();
		groundLeft.layer = CollisionLayer.terrain;
		groundLeft.halfExtents = Vect.Create(10, 75);
		groundLeft.position.set(50, 175);
		groundLeft.CreateWireframe(0xFFFFFF, 1);
		
		let groundRight = new AABBCollider();
		groundRight.layer = CollisionLayer.terrain;
		groundRight.halfExtents = Vect.Create(10, 75);
		groundRight.position.set(350, 175);
		groundRight.CreateWireframe(0xFFFFFF, 1);

		let groundTop = new AABBCollider();
		groundTop.layer = CollisionLayer.terrain;
		groundTop.halfExtents = Vect.Create(150, 10);
		groundTop.position.set(200, 100);
		groundTop.CreateWireframe(0xFFFFFF, 1);
		
		let groundMid = new AABBCollider();
		groundMid.layer = CollisionLayer.terrain;
		groundMid.halfExtents = Vect.Create(20, 20);
		groundMid.position.set(200, 175);
		groundMid.CreateWireframe(0xFFFFFF, 1);

		this.addChild(groundBottom, groundLeft, groundRight, groundTop, groundMid);
	}
}