///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Collider, CollisionLayer } from './collider';
import { Collision, ICollision } from './iCollision';
import { Ray } from './ray';
import { ICirclecastResult, IRaycastResult } from './raycastResult';

/** 
 * meant to be an object that separates a set of colliders into spatial partitions, and then only 
 * checks collisions between those who share a partition instead of checking each collider against
 * every other collider. This would be done for optomization, but I haven't actually implemented 
 * it yet.
 */
export class ColliderPartitions {

	constructor() { }

	private _forcedCols: Array<Collider>;
	public any: Array<Collider> = new Array<Collider>();
	public terrain: Array<Collider> = new Array<Collider>();
	public actors: Array<Collider> = new Array<Collider>();
	public props: Array<Collider> = new Array<Collider>();
	public projectiles: Array<Collider> = new Array<Collider>();
	public effects: Array<Collider> = new Array<Collider>();

	// ---------------------------------------------------------------------------------------------
	public GetCompatibleCollisionLayers(layer: CollisionLayer): Array<Array<Collider>>{
		let r = new Array<Array<Collider>>();

		// TODO finish implementation
		switch(layer){
			case CollisionLayer.none:
				r.push(this.any, this.terrain, this.actors, this.props, this.projectiles, this.effects);
				break;
			case CollisionLayer.actors: 
				r.push(this.terrain, this.actors, this.props, this.projectiles);
				break;
			case CollisionLayer.projectiles: 
				r.push(this.terrain, this.actors, this.props);
				break;
			case CollisionLayer.terrain: 
				r.push(this.any, this.actors, this.projectiles, this.props);
				break;
		}

		return r;
	}

	public AddCollider(collider: Collider, layer: CollisionLayer = null): void{
		if(layer != null){
			collider.layer = layer;
		}

		let array = this.any;
		switch(collider.layer){
			case CollisionLayer.terrain: array = this.terrain; break;
			case CollisionLayer.actors: array = this.actors; break;
			case CollisionLayer.props: array = this.props; break;
			case CollisionLayer.projectiles: array = this.projectiles; break;
			case CollisionLayer.effects: array = this.effects; break;
		}

		array.push(collider);
	}

	public RemoveCollider(collider: Collider): void{

		let array = this.any;
		switch(collider.layer){
			case CollisionLayer.terrain: array = this.terrain; break;
			case CollisionLayer.actors: array = this.actors; break;
			case CollisionLayer.props: array = this.props; break;
			case CollisionLayer.projectiles: array = this.projectiles; break;
			case CollisionLayer.effects: array = this.effects; break;
		}

		let index = array.indexOf(collider);
		if(index >= 0){
			array.splice(index, 1);
		}
	}

	public ForceCollision(colliderA: Collider, colliderB: Collider){

		if(this._forcedCols == null){
			this._forcedCols = new Array<Collider>();
		}

		this._forcedCols.push(colliderA, colliderB);
	}

	private ContainsForcedCollision(colA: Collider, colB: Collider){
		
		// iterate through each forced collision pair
		if(this._forcedCols != null) for(let i = this._forcedCols.length - 1; i > 0; i -= 2){
			
			if(
				(this._forcedCols[i - 1] == colA && this._forcedCols[i] == colB) ||
				(this._forcedCols[i - 1] == colB && this._forcedCols[i] == colA) 
				)
				return true;
		}

		return false;
	}

	public FindOverlappingColliders(col: Collider): Array<Collider>{

		let r = new Array<Collider>();
		let layers = this.GetCompatibleCollisionLayers(col.layer);
		
		for(let i0 = layers.length - 1; i0 >= 0; i0--){
			for(let i1 = layers[i0].length - 1; i1 >= 0; i1--){
				if(col.OverlapsCollider(layers[i0][i1])){
					r.push(layers[i0][i1]);
				}
			}
		}

		return r;
	}

	public FindCollisions(): Array<ICollision>{

		let r = new Array<ICollision>();

		// iterate through each terrain collider
		for(let i0 = this.terrain.length - 1; i0 >= 0; i0--){

			let colA = this.terrain[i0];

			// check collisions for terrain vs actors
			for(let i1 = this.actors.length - 1; i1 >= 0; i1--){

				let colB = this.actors[i1];
				
				if(this.ContainsForcedCollision(colA, colB))
					continue;

				if(colA.OverlapsCollider(colB))
					r.push(colA.GetCollision(colB));
			}
			
			// check collisions for terrain vs projectiles
			for(let i1 = this.projectiles.length - 1; i1 >= 0; i1--){

				let colB = this.projectiles[i1];
				
				if(this.ContainsForcedCollision(colA, colB))
					continue;
					
				if(colA.OverlapsCollider(colB))
					r.push(colA.GetCollision(colB));
			}
		}

		// iterat through all actor colliders
		for(let i0 = this.projectiles.length - 1; i0 >= 0; i0--){

			let colA = this.projectiles[i0];

			// check collisions for projectiles vs actors
			for(let i1 = this.actors.length - 1; i1 >= 0; i1--){

				let colB = this.actors[i1];
				
				if(this.ContainsForcedCollision(colA, colB))
					continue;
					
				if(colA.OverlapsCollider(colB))
					r.push(colA.GetCollision(colB));
			}
		}

		// iterate through each forced collision
		if(this._forcedCols != null) {
			for(let i = this._forcedCols.length - 1; i > 0; i -= 2){

				let colA = this._forcedCols[i - 1];
				let colB = this._forcedCols[i];
				r.push(colA.GetCollision(colB));
			}

			// clear the forced collisions
			this._forcedCols.splice(0, this._forcedCols.length);
		}


		return r;
	}

	public Raycast(ray: Ray, layer: CollisionLayer): Array<IRaycastResult>{
		let r = new Array<IRaycastResult>();

		// iterate through each layer that the raycast can collide with
		let layers = this.GetCompatibleCollisionLayers(layer);
		for(let i0 = layers.length - 1; i0 >= 0; i0--){

			// iterate through each collider layer
			let layer = layers[i0];
			for(let i1 = layer.length - 1; i1 >= 0; i1--){

				// calculate raycast and append to results if necessary
				let collider = layer[i1];
				let result = collider.RayCast(ray);
				if(result != null && (result.didEnter || result.didExit)){
					r.push(result);
				}
			}
		}

		return r;
	}

	public CircleCast(ray: Ray, radius: number, layer: CollisionLayer): Array<ICirclecastResult>{
		let r = new Array<ICirclecastResult>();

		// iterate through each layer that the raycast can collide with
		let layers = this.GetCompatibleCollisionLayers(layer);
		for(let i0 = layers.length - 1; i0 >= 0; i0--){

			// iterate through each collider layer
			let layer = layers[i0];
			for(let i1 = layer.length - 1; i1 >= 0; i1--){

				// calculate circlecast and append to results if necessary
				let collider = layer[i1];
				let result = collider.CircleCast(ray, radius);
				if(result != null && (result.didEnter || result.didExit)){
					r.push(result);
				}
			}
		}

		return r;
	}
}