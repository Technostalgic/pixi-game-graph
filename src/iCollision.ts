///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

import { Collider } from "./collider";
import { IVect } from "./math";

export interface ICollision{
	
	colliderA: Collider;
	colliderB: Collider;

	collisionPoint: IVect;
	directionNormal: IVect;
	penetration: number;
}

export namespace Collision{

	/**
	 * Create a collision with the specified data
	 * @param a the primary collider involved in the collision
	 * @param b the other collider involved in the collision
	 * @param normal the normalized direciton vector that the 2-way collision force is directed in
	 * @param point the centroid of the collision overlap area
	 * @param penetration how much much the colliders are overlapping in the specified normal 
	 * 	direction
	 */
	export function Create
	(a: Collider, b: Collider, normal: IVect, point: IVect, penetration: number): ICollision{
		return{
			colliderA: a, colliderB: b,
			directionNormal: normal, collisionPoint: point,
			penetration: penetration
		};
	}

	/**
	 * Recalculates a collision if either of the properties of the colliders have been modified
	 * @param collision the collision to recalculate
	 */
	export function Recalculate(collision: ICollision): void{
		
		let r = collision.colliderA.GetCollision(collision.colliderB);
		collision.colliderA = r.colliderA;
		collision.colliderB = r.colliderB;
		collision.collisionPoint = r.collisionPoint;
		collision.directionNormal = r.directionNormal;
		collision.penetration = r.penetration;
	}

	/**
	 * returns the collider involved in the collsision who is not the specified self, or null if the
	 * specified self isn't involved in the collision at all
	 * @param collision the collision to get the collider of
	 * @param self the colldier that we do not want
	 * @returns 
	 */
	export function OtherCollider(collision: ICollision, self: Collider){
		if(collision.colliderA == self) return collision.colliderB;
		if(collision.colliderB == self) return collision.colliderA;
		return null;
	}
}