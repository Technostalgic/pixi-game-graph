///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from "pixi.js"
import { AABBCollider, Collider } from "./collider";
import { GameEvent } from "./gameEvent";
import { ICollider } from "./iCollider";
import { IRect, IVect, Rect, Vect } from "./math";
import { SceneEntity } from "./sceneEntity";
import { UIContainer } from "./uiContainer";

export class UIElement extends SceneEntity{

	public constructor(){
		super();
		this._name = "Ui Element";
	}

	public get ParentContainer(): UIContainer { return this.parentScene as UIContainer; };
	
	private _panelGraphic: PIXI.Graphics = null;
	protected _panelCollider: AABBCollider = null;

	public uiAnchor: IVect = Vect.Create(0, 0);
	public uiPivot: IVect = Vect.Create(0, 0);
	public uiOffset: IVect = Vect.Create(0, 0);
	public uiSize: IVect = Vect.Create(0, 0);
	public uiSizeOffset: IVect = Vect.Create(0, 0);

	// public get uiOffset(): IVect { return this.position; }
	// public set uiOffset(val) {
	// 	let dif = Vect.Subtract(val, this.uiOffset);
	// 	let tpos = this.position;
	// 	this.position.set(tpos.x + dif.x, tpos.y + dif.y);
	// }

	// ---------------------------------------------------------------------------------------------
	public AlignedCenter(): UIElement{
		this.uiAnchor.x = 0.5;
		this.uiAnchor.y = 0.5;
		this.uiPivot.x = 0.5;
		this.uiPivot.y = 0.5;
		return this;
	}

	/**
	 * updates the object position to represent the appropriate ui position relative to parents
	 * @param deepUpdate whether or not all children should also recursively update their positions
	 */
	public UpdateUiPosition(deepUpdate: boolean = true): void{

		// update to appropriate position according to the ui rect
		this.GetUiPosition();

		if(!deepUpdate) return;

		// deep recursive update through children
		let entities = this.entities;
		for(let i = 0; i < entities.length; i++){
			let ent = entities[i];
			if(ent instanceof UIElement){
				ent.UpdateUiPosition(deepUpdate);
			}
		}
	}

	// ---------------------------------------------------------------------------------------------
	public GetUiPosition(): IVect{
		
		// get the parent rect
		let parentRect = this.GetParentUiRect();

		// calculate the screenspace coordinates from parent rect
		let tpos = Vect.Multiply(this.uiAnchor, parentRect.size);
		tpos.x += this.uiOffset.x;
		tpos.y += this.uiOffset.y;
		tpos.x += parentRect.position.x;
		tpos.y += parentRect.position.y;

		let tsiz = Vect.Multiply(this.uiSize, parentRect.size);
		tsiz.x += this.uiSizeOffset.x;
		tsiz.y += this.uiSizeOffset.y;

		tpos.x -= tsiz.x * this.uiPivot.x;
		tpos.y -= tsiz.y * this.uiPivot.y;

		// update local entity position
		this.globalPosition = tpos;
		return tpos;
	}

	public GetUiSize(): IVect{
		
		// get the parent rect
		let parentRect = this.GetParentUiRect();
		
		let tsiz = Vect.Multiply(this.uiSize, parentRect.size);
		tsiz.x += this.uiSizeOffset.x;
		tsiz.y += this.uiSizeOffset.y;

		return tsiz;
	}

	public GetParentUiRect(): IRect{
		
		let parentRect = null;
		if(this.parentEntity instanceof UIContainer){
			parentRect = this.ParentContainer.uiRect;
		}
		else if(this.parentEntity instanceof UIElement){
			parentRect = this.parentEntity.GetUiRect();
		}
		else if(this.parentEntity == null){
			parentRect = Rect.Create(Vect.Create(), Vect.Create());
		}
		else{
			parentRect = Rect.Create(this.parentEntity.globalPosition, Vect.Create(0, 0));
		}

		return parentRect;
	}

	public GetUiRect(): IRect{
		
		// get the parent rect
		let parentRect = this.GetParentUiRect();

		// calculate the scrrenspace coordinates from parent rect
		let tpos = Vect.Multiply(this.uiAnchor, parentRect.size);
		tpos.x += this.uiOffset.x;
		tpos.y += this.uiOffset.y;
		tpos.x += parentRect.position.x;
		tpos.y += parentRect.position.y;

		// calculate size from parent rect size
		let tsiz = Vect.Multiply(this.uiSize, parentRect.size);
		tsiz.x += this.uiSizeOffset.x;
		tsiz.y += this.uiSizeOffset.y;
		
		// calculate top left of rect based on pivot
		let topLeft = Vect.Subtract(tpos, Vect.Multiply(this.uiPivot, tsiz));
		
		// update local entity position
		tpos.x -= tsiz.x * this.uiPivot.x;
		tpos.y -= tsiz.y * this.uiPivot.y;
		this.globalPosition = tpos;

		// create the rect from the calculated coordinates
		let rect = Rect.Create(topLeft, tsiz);
		return rect;
	}

	/** @inheritdoc */
	public override addChild<T extends PIXI.DisplayObject>
	(...childs: [T, ...PIXI.DisplayObject[]]): T{
		
		let r = super.addChild(...childs);
		
		for(let i = childs.length - 1; i >= 0; i--){
			let child = childs[i];
			if(child instanceof UIElement){
				child.UpdateUiPosition(true);
			}
		}
		
		return r;
	}
	

	// ---------------------------------------------------------------------------------------------
	/**
	 * expands the sizeOffset of the UI element to fit all the UI element children inside it
	 * @param paddingX how much horizontal padding between children and edge of element
	 * @param paddingY how much vertical padding between children and edge of element 
	 */
	public ExpandToFitChildren(paddingX: number = 0, paddingY: number = paddingX): void{
		
		let oRect = this.GetUiRect();
		let tRect = Rect.Clone(oRect);

		let ents = this.entities;
		for(let i = ents.length - 1; i >= 0; i--){

			let ent = ents[i];
			if(ent instanceof UIElement){

				let childRect = ent.GetUiRect();
				Rect.ExpandToOverlap(tRect, childRect);
			}
		}

		let difX = tRect.size.x - oRect.size.x;
		let difY = tRect.size.y - oRect.size.y;
		if(oRect.position.x > tRect.position.x) difX += paddingX;
		if(oRect.position.x + oRect.size.x < tRect.position.x + tRect.size.x) difX += paddingX;
		if(oRect.position.y > tRect.position.y) difY += paddingY;
		if(oRect.position.y + oRect.size.y < tRect.position.y + tRect.size.y) difY += paddingY;
		
		this.uiSizeOffset.x += difX;
		this.uiSizeOffset.y += difY;
	}
	
	public UpdatePanelGraphic(): PIXI.Graphics{

		// create or get the graphics object
		let graphics = null;
		if(this._panelGraphic != null){
			graphics = this._panelGraphic;
			graphics.clear();
		}
		else{
			graphics = new PIXI.Graphics();
			this.addChild(graphics);
			this._panelGraphic = graphics;
		}

		let uiRect = this.GetUiRect();
		let tpos = Vect.Subtract(uiRect.position, this.globalPosition);
		
		graphics.lineStyle({
			color: 0xFFFFFF,
			width: 1
		});
		graphics.beginFill(0x000000, 0.5);
		graphics.drawRect(tpos.x, tpos.y, uiRect.size.x - 1, uiRect.size.y - 1);
		graphics.endFill();

		return graphics;
	}

	public UpdatePanelCollider(): Collider{

		let collider = null;
		if(this._panelCollider == null){
			collider = new AABBCollider();
			this.addChild(collider);
		}
		else{
			collider = this._panelCollider;
		}
		
		collider.SetRect(this.GetUiRect());
		return collider;
	}
}

export class UIInteractiveElement extends UIElement implements ICollider{

	public constructor(){
		super();
		this._name = "Ui Interactive Element";
	}

	public get collider() { 
		if(this._panelCollider == null)
			this.UpdatePanelCollider();
		return this._panelCollider; 
	}
	public set collider(val){
		this._panelCollider = val;
		this.addChild(val);
	}

	/** this event is fired when a player selects the UI element */
	public get SelectAction() { return this._selectAction; }
	private _selectAction: GameEvent<number> = new GameEvent<number>();
}

export class UITextElement extends UIElement{

	/** create from pixi text object */
	public constructor(text: PIXI.Text){
		super();

		this.addChild(text);
		this._text = text;
		this.MatchSizeOffsetToText(0, 0);
		this._name = "Ui Text '" + text.text + "'";
	}

	public static readonly DEFAULT_STYLE = {
		fontFamily: "Consolas",
		fontSize: 12,
		fill: 0xFFFFFF,
		stroke: 0x000000,
		strokeThickness: 1
	}
	
	/**
	 * create a text element from the specified string
	 * @param text the string that the text element will contain
	 * @param style the style to format the text by
	 */
	public static CreateFromString(
		text: string, 
		style: Partial<PIXI.TextStyle> = this.DEFAULT_STYLE
	): UITextElement{
		
		let txt = new PIXI.Text(text, style);
		return new UITextElement(txt);
	}

	// ---------------------------------------------------------------------------------------------
	private _text: PIXI.Text;
	public get text() { return this._text; }

	public textAnchor: IVect = Vect.Create(0, 0);

	// ---------------------------------------------------------------------------------------------
	public override UpdateUiPosition(deepUpdate: boolean = true): void {
		super.UpdateUiPosition(deepUpdate);
		this.UpdateTextPosition();
	}

	public override AlignedCenter(): UIElement {
		super.AlignedCenter();
		this.textAnchor.x = 0.5;
		this.textAnchor.y = 0.5;
		return this;
	}

	public UpdateTextPosition(){

		// get some data about the text and ui space for calculations
		let rect = this.GetUiRect();
		let textSize = Vect.Create(this._text.width, this._text.height);

		// calculate the offset from the top left of the ui rect, based on the text achor
		let toff = Vect.Create();
		toff.x = this.textAnchor.x * (rect.size.x - textSize.x);
		toff.y = this.textAnchor.y * (rect.size.y - textSize.y);

		// apply the position
		SceneEntity.SetGlobalPosition(this._text,
			Vect.Create(
				rect.position.x + toff.x, 
				rect.position.y + toff.y
		));
	}

	// ---------------------------------------------------------------------------------------------
	/**
	 * Set the size offset of the ui element so it matches the width and height of the text
	 * @param paddingX the amount of space between the text and the element's leftright edges
	 * @param paddingY the amount of space between the text and the element's top/bottom edges
	 */
	public MatchSizeOffsetToText(paddingX: number, paddingY: number	){
		this.uiSizeOffset = Vect.Create(
			this._text.width + paddingX, this._text.height + paddingY
		);
	}

	public SetTextString(text: string){
		this._text.text = text;
		this.UpdateTextPosition();
	}
}