///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Effect } from './effect';

/**
 * an AnimatedEffect object is any sort of effect that can be played from a spritesheet
 */
export class AnimatedEffect extends Effect{

	public constructor() { super(); }

	private _spriteAnim: PIXI.AnimatedSprite;
	private _sprite: PIXI.Sprite;
	private _frameTimer: number = 0;
	private _frameDelay: number = 0;
	private _currentFrame: number = 0;

	/**
	 * create an animated effect from the provided information
	 * @param spriteAnim the sprite for the effect to animate
	 * @param frameDelay the amount of seconds that each frame will be shown for
	 */
	public static Create(
		spriteAnim: PIXI.AnimatedSprite, frameDelay: number, color: number = 0xFFFFFF
	): AnimatedEffect{

		let r = new AnimatedEffect();
		
		r._spriteAnim = spriteAnim;
		r._sprite = new PIXI.Sprite(spriteAnim.texture);
		r._sprite.anchor.set(spriteAnim.anchor.x, spriteAnim.anchor.y);
		r._sprite.tint = color;

		r._frameDelay = frameDelay;
		r._frameTimer = frameDelay;
		r._spriteAnim.gotoAndStop(0);
		r.addChild(r._sprite);
		
		return r;
	}

	public override Update(deltaTime: number): void {
		super.Update(deltaTime);

		// deduct frame timer and test to see if it has fully elapsed
		this._frameTimer -= deltaTime;
		if(this._frameTimer <= 0){

			// reset frame timer and get next frame
			this._frameTimer = this._frameDelay;
			this._currentFrame += 1;

			// if next frame exceeds total frame count, animaiton is complete, so we remove the 
			// effect
			if(this._currentFrame >= this._spriteAnim.totalFrames){
				this.RemoveFromScene();
			}

			// increment the frame
			else{
				let tex = this._spriteAnim.textures[this._currentFrame] as PIXI.Texture<PIXI.Resource>;
				this._sprite.texture.frame = new PIXI.Rectangle(
					tex.frame.x, tex.frame.y, tex.frame.width, tex.frame.height
				);
			}
		}
	}
}