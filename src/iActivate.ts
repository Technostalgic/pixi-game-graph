///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/** Interface to allow a scene entity to be able to be activated or deactivated */
export interface IActivate{
	
	/** Whether or not the bject is currently active in the scene */
	get isActivated(): boolean;

	/**
	 * set whether or not the object is active on the scene
	 * @param active 
	 */
	SetActivated(active: boolean): void;
}

export namespace IActivate{
	
	/** Type checking discriminator for IActivate */
	export function implementedIn(obj: any): obj is IActivate{
		return(
			"isActive" in obj &&
			"SetActive" in obj
		);
	}
}