///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Game } from './game';
import { GameEvent } from './gameEvent';
import { IVect, Vect } from './math';
import { Scene } from './scene';

export class SceneEntity extends PIXI.Container{

	public static readonly TYPE_CHILDREF = "_childRef";
	public static readonly SFIELD_TYPE = "type";
	public static readonly SFIELD_CHILDREN = "_entities";
	public static readonly SFIELD_CHILDINDEX = "childIndex";

	public constructor(){ 
		super(); 

		this._entities = new Array<SceneEntity>();
	}

	// ---------------------------------------------------------------------------------------------
	private _removed: boolean = false;
	protected _name: string = null;
	protected _parentScene: Scene = null;
	protected _parentEntity: SceneEntity = null;
	protected _entities: Array<SceneEntity>;
	
	public get name(): string{

		if(this._name != null)
			return this._name;

		if(this.parent != null && this.parent instanceof SceneEntity)
			return this.parent.name + "_chld";
		
		return "Entity";
	}
	public set name(nm){
		this._name = nm;
	}

	public get removed() { return this._removed; }
	public get parentGame() { return this._parentScene.parentGame; }
	public get parentScene() { return this._parentScene; }
	public get parentEntity(): SceneEntity { 
		return this._parentEntity ?? (this._parentEntity = (this.parent as SceneEntity)); 
	}
	public get entities() { return this._entities; }

	public get globalPosition(): IVect { return this.toGlobal(Vect.Create()); }
	public set globalPosition(pos) {
		let localPos = this.parent?.toLocal(pos) ?? Vect.Clone(pos);
		this.position.set(localPos.x, localPos.y);
	}

	// ---------------------------------------------------------------------------------------------
	public static GetGlobalPosition(obj: PIXI.Container){
		return obj.toGlobal(Vect.Create());
	}
	public static SetGlobalPosition(obj: PIXI.Container, pos: IVect){
		let localPos = obj.parent.toLocal(pos);
		obj.position.set(localPos.x, localPos.y);
	}

	///---------------------------------------------------------------------------------------------
	/// Hierarchy

	/** called when this entity is added to another scene entity as a child */
	protected OnAddedToParent(parent: SceneEntity): void{
		this._parentEntity = parent;
	}

	/** called when added to a new scene */
	protected OnAddedToScene(scene: Scene): void{
		this._parentScene = scene;
	}

	/** called when removed from a scene */
	protected OnRemovedFromScene(scene: Scene): void{ }

	/**
	 * for all child entities, ensure that they are also removed from the scene if this entity is
	 * removed from a scene
	 * @param scene 
	 */
	private UpdateChildrenRemovedFromScene(scene: Scene): void{

		// iterate through each child entity
		for(let i = 0; i < this.entities.length; i++){

			let ent = this.entities[i];
			ent.OnRemovedFromScene(scene);
			ent.UpdateChildrenRemovedFromScene(scene);
		}
	}

	/**
	 * for all the entities that are children of this object, ensure that they share the
	 * same parent scene as this object
	 */
	private UpdateChildrenParentRef(): void{
		for(let i = this._entities.length - 1; i >= 0; i--){

			// update entity new parent scene
			let ent = this._entities[i];
			ent.OnAddedToScene(this.parentScene);
			ent.OnAddedToParent(this);
			ent.UpdateChildrenParentRef();
		}
	}

	/** @inheritdoc */
	public override addChild<T extends PIXI.DisplayObject>
	(...childs: [T, ...PIXI.DisplayObject[]]): T{
		
		// base functionality
		let t = super.addChild<T>(...childs);
		
		// iterate through each child provided
		for(let i = 0; i < childs.length; i++){
			
			// if it is not a scene entity, only perform base class addChild functionality
			if(!(childs[i] instanceof SceneEntity))
				continue;
			
			// otherwise, provide scene entity data
			let child = childs[i] as SceneEntity;
			
			// parent the child entity to our scene, and all of it's children too
			child.OnAddedToScene(this.parentScene);
			child.OnAddedToParent(this);
			child.UpdateChildrenParentRef();
			
			// add to child entities
			this._entities.push(child);
		}
		
		return t;
	}

	/** @inheritdoc */
	public override removeChild<T extends PIXI.DisplayObject[]>
	(...childs: T): T[0]{

		// iterate through each child provided
		for(let i = 0; i < childs.length; i++){

			// if it is not a scene entity, only perform base class removeChild functionality
			if(!(childs[0] instanceof SceneEntity))
				continue;
	
			let child = childs[0] as SceneEntity;
			let oscn = child.parentScene;

			child._parentEntity = null;
			child._parentScene = null;

			if(oscn != null){
				child.OnRemovedFromScene(oscn);
				child.UpdateChildrenRemovedFromScene(oscn);
			}
			
			// remove from child entities
			let index = this._entities.indexOf(child);
			this._entities.splice(index, 1);
		}
		
		// base functionality
		return super.removeChild(...childs);
	}

	/** @inheritdoc */
	public override destroy(options?: PIXI.IDestroyOptions | boolean): void{

		let pent = this.parentEntity;

		this._parentEntity = null;
		super.destroy(options);

		if(pent == null)
			return;
		
		let pind = pent._entities.indexOf(this);
		if(pind >= 0)
			pent._entities.splice(pind, 1);
		
		this._parentScene = null;
	}

	/**
	 * Removes the entity from the scene at the end of the update cycle (before being rendered)
	 */
	public RemoveFromScene(){

		this.parentScene.entitiesToRemove.push(this);
		this._removed = true;
	}

	/**
	 * returns the first child entity of the specified type
	 * @param type the type (constructor function) value to look for, must be the same type passed 
	 * 	in as the generic parameter
	 */
	public GetChildOfType<T extends SceneEntity>(type: Function): T{

		for(let i = 0; i < this.entities.length; i++){
			if(this.entities[i] instanceof type){
				return this.entities[i] as T;
			}
		}

		return null;
	}

	///---------------------------------------------------------------------------------------------
	/// Scene Methods

	public Update(deltaTime: number): void{ 

		// iterate through each scene entity child in the entity container
		for(let i = 0; i < this._entities.length; i++){

			// update the scene entity's game logic
			this._entities[i].Update(deltaTime);
		}
	}

	public Draw(renderer: PIXI.Renderer): void{ }

	///---------------------------------------------------------------------------------------------
	/// Cloning

	protected CopyFields(source: SceneEntity): void{

		let pos = source.position;
		this.position.set(pos.x, pos.y);
		this._name = source._name;
		this._parentEntity = source._parentEntity;
	}

	protected CopyChildren(source: SceneEntity): void{

		let childs = source.entities;
		for(let i = 0; i < childs.length; i++){
			
			let childClone = childs[i].Clone();
			if(childClone != null) this.addChild(childClone);
		}
	}

	/** creates a clone of the object and returns it */
	public Clone(): SceneEntity{

		let r = new (Object.getPrototypeOf(this).constructor)() as SceneEntity;
		r.CopyChildren(this);
		r.CopyFields(this);

		return r;
	}

	///---------------------------------------------------------------------------------------------
	/// Serialization

	/** returns a JSON serializable object that has all the necessary data to recreate itself */
	public GetSerializedObject(): any{

		// get the serialized type name from the game's type registry
		let serializedTypeName = 
			this.parentGame.GetRegisteredTypeName(Object.getPrototypeOf(this).constructor);
		if(serializedTypeName == null){
			console.error(this);
			throw (
				"The type for '" + 
				Object.getPrototypeOf(this).constructor.name + 
				"' has not been registered in the game"
			);
		}

		// create an anonymous object to store all our fields into
		let r:any = {};
		r[SceneEntity.SFIELD_TYPE] = serializedTypeName;
		
		// create a field for, serialize and store all the children of this scene entity
		r[SceneEntity.SFIELD_CHILDREN] = [];
		for(let i = 0; i < this.entities.length; i++){
			r[SceneEntity.SFIELD_CHILDREN].push(this.entities[i].GetSerializedObject());
		}

		// store the object position and name
		r._name = this._name;
		r.position = {
			x: this.position.x,
			y: this.position.y
		};

		// serialize and store the rest of the object fields
		this.SerializeFieldsInto(r);

		return r;
	}

	/**
	 * copy all the fields of the entity into an anonymous object for serialization
	 * @param obj the object to copy our fields into
	 */
	protected SerializeFieldsInto(obj: any, omittedTypes: Array<any> = []){
		
		let dif = new SceneEntity() as any;
		let untypedSelf = this as any;

		omittedTypes.push( ... [
			Function,
			PIXI.DisplayObject,
			GameEvent,
		]);

		// iterate though each field we have and copy the necesary ones over to our 
		// serialized object
		fieldCopy: for(let field in untypedSelf){

			// we don't want to copy any of these fields or
			// any field if it's shared with the base entity type (so we don't copy over a
			// bunch of useless redundant garbage into the serialized object)
			if(
				untypedSelf[field] === undefined || 
				field === "_parentGame" ||
				field === "_parentScene" ||
				dif[field] !== undefined
			){
				continue fieldCopy;
			}

			// dont copy omitted types
			for(let i = omittedTypes.length - 1; i >= 0; i--){
				if(untypedSelf[field] instanceof omittedTypes[i]){
					continue fieldCopy;
				}
			}

			// if it's a scene entity field, it must be a child reference
			if(untypedSelf[field] instanceof SceneEntity){

				// if the field references an entity outside this object's children, don't store it
				let crIndex = this.entities.indexOf(untypedSelf[field]);
				if(crIndex < 0)
					continue fieldCopy;
				
				// serialize a field to the reference into the object
				let childRef: any = {};
				childRef.type = SceneEntity.TYPE_CHILDREF;
				childRef[SceneEntity.SFIELD_CHILDINDEX] = crIndex;
				obj[field] = childRef;

				continue fieldCopy;
			}

			obj[field] = untypedSelf[field];
		}
	}

	/**
	 * copy the field values from a serialized object
	 * @param obj the serialized object to parse from
	 */
	protected CopyFieldsFromSerialized(obj: any, gameRef: Game): void{

		// cast self to anonymous object
		let untypedSelf = this as any;

		// iterate through each of the object's fields
		for(let fieldName in obj){

			// don't copy the children
			if(fieldName == SceneEntity.SFIELD_CHILDREN){
				continue;
			}

			// copy the field from the serialized object if we have a field of the same name
			if(untypedSelf[fieldName] !== undefined){
				untypedSelf[fieldName] = SceneEntity.GetDeserializedField(
					obj, obj[fieldName], gameRef
				);
			}
		}
	}

	/**
	 * copy the children into this object from a serialized object
	 * @param obj the serialized object whose child list to parse and copy
	 */
	protected CopyChildrenFromSerialized(obj: any, gameRef: Game): void{
		if(obj[SceneEntity.SFIELD_CHILDREN] === undefined) return;

		for(let i = 0; i < obj[SceneEntity.SFIELD_CHILDREN].length; i++){

			let child = SceneEntity.GetDeserializedObject(
				obj[SceneEntity.SFIELD_CHILDREN][i], gameRef
			);
			this.entities.push(child);
		}
	}

	/** called immediately after the object is deserialized */
	protected OnPostDeserialize(){ }

	/**
	 * Deserialize an object based on it's 'type' property in the scope of the specified game
	 * @param obj the object to deserialized
	 * @param gameRef the reference to the game who's type registry will be used
	 */
	public static GetDeserializedObject(obj: any, gameRef: Game): SceneEntity{

		if(obj[SceneEntity.SFIELD_TYPE] === undefined) return null;
		if(obj[SceneEntity.SFIELD_TYPE] === this.TYPE_CHILDREF){
			throw (
				"Objects cannot be parsed as children without a reference to their sibling list," + 
				"use SceneEntity.GetDeserializedField(...) instead"
			);
		}

		// search for the serialized type
		let type = gameRef.GetRegisteredType(obj[SceneEntity.SFIELD_TYPE]);
		let ur = new type();
		if(ur._parentGame === null){
			ur._parentGame = gameRef;
		}

		let r = ur as SceneEntity;

		// copy/deserialize the children and object fields
		r.CopyChildrenFromSerialized(obj, gameRef);
		r.CopyFieldsFromSerialized(obj, gameRef);

		// truncate children that are null (probably did not parse properly)
		for(let i = r[SceneEntity.SFIELD_CHILDREN].length - 1; i >= 0; i--){
			if(r[SceneEntity.SFIELD_CHILDREN][i] == null){
				r[SceneEntity.SFIELD_CHILDREN].splice(i, 1);
			}
		}

		r?.OnPostDeserialize();

		return r;
	}

	/**
	 * Similar to GetDeserializedObject, with different rules for what is returned if the data type 
	 * isn't a serialized SceneEntity. In this case we'll want to return the value itself if the 
	 * object doesn't appear to be a serialized scen entity. This is because the value is probably a
	 * number, string, array or other literal type that we want to handle as a value type
	 * @param fieldObj the reference to the object to deserialize
	 * @param gameRef the reference to the game who's type registry will be used
	 */
	public static GetDeserializedField(fieldOwner: any, fieldObj: any, gameRef: Game){
		if(fieldObj == null) return null;

		// parse the object as a reference to one of the object's children
		if(fieldObj[SceneEntity.SFIELD_TYPE] === this.TYPE_CHILDREF){
			let child = fieldOwner.childs[fieldObj[this.SFIELD_CHILDINDEX]];
			return child;
		}

		// parse the
		let obj = this.GetDeserializedObject(fieldObj, gameRef);
		
		if(obj == null)
			return fieldObj;

		return fieldObj;
	}
}
