///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { Collider } from "./collider";
import { ICollision } from "./iCollision";
import { ICollider } from "./iCollider";
import { IVect, Vect } from "./math";
import { SceneEntity } from "./sceneEntity";

/**
 * A scene entity that simulates basic physics movement and collision reactions (collision 
 * reactions still need some work - currently only static collisions are properly calculated)
 */
export class PhysicsEntity extends SceneEntity implements ICollider{

	public constructor(){ super(); }

	protected _collider: Collider;
	protected _velocity: IVect = Vect.Create();
	private _mass: number = 1;
	private _inertia: number = 1;
	
	public get collider() { return this._collider; }
	
	public get velocity(){ return this._velocity; }
	public set velocity(val){ 
		this._velocity.x = val.x;
		this._velocity.y = val.y;
	}

	public get mass(){ return this._mass; }
	public set mass(val){
		this._mass = val;

		if(val > 0) this._inertia = 1 / this.mass;
		else this._inertia = Number.POSITIVE_INFINITY;
	}

	public get inertia(){ return this._inertia; }
	public get momentum(){
		return Vect.MultiplyScalar(this.velocity, this.mass);
	}
	
	public override Update(deltaTime: number): void{
		super.Update(deltaTime);
		
		this.Simulate(deltaTime);
	}

	/**
	 * accelerate the object toward a specified velocity
	 * @param velocity the velocity to approach
	 * @param maxAccel the maximum magnitude of acceleration to apply toward the velocity
	 */
	public AccelerateToVelocity(vel: IVect, maxAccel: number){
		
		// if target velocity is reachable in this step, set velocity to target velocity
		let dif = Vect.Subtract(vel, this.velocity);
		if(Vect.Magnitude(dif) < maxAccel){
			this._velocity.x = vel.x;
			this._velocity.y = vel.y;
			return;
		}

		// apply maximum possible acceleration if velocity is not reachable
		let acc = Vect.MultiplyScalar(Vect.Normalize(dif), maxAccel);
		this._velocity.x += acc.x;
		this._velocity.y += acc.y;
	}

	/**
	 * applies an impulse to to the physics object
	 * @param impulse the impulse to apply
	 */
	public ApplyImpulse(impulse: IVect){

		let acc = Vect.MultiplyScalar(impulse, this._inertia);
		this._velocity = Vect.Add(this._velocity, acc);
	}

	/**
	 * applies a force to the object over a specified time
	 * @param force the force to apply
	 * @param deltaTime the amount of time to apply the force over (note, the cumulative force is 
	 * 	applied instantly, even if a long deltaTime is specified)
	 */
	public ApplyForce(force: IVect, deltaTime: number){

		let acc = Vect.MultiplyScalar(force, this.inertia * deltaTime);
		this._velocity = Vect.Add(this._velocity, acc);
	}

	public ResolveSolidCollision(collision: ICollision): void{

		let norm = collision.directionNormal;
		if(this._collider == collision.colliderB){
			norm = Vect.Invert(norm);
		}

		// add velocity against collision normal
		let dot = Vect.Dot(this._velocity, norm);
		if(dot < 0){
			let dVel = Vect.Project(this._velocity, norm);
			dVel = Vect.Invert(dVel);
			this._velocity = Vect.Add(this._velocity, dVel);
		}

		// modify position to move entity outside of collider
		let pos = this.globalPosition;
		this.position.set(
			pos.x + norm.x * collision.penetration,
			pos.y + norm.y * collision.penetration
		);
	}

	public Simulate(deltaTime: number){

		// translate position by velocity applied over delta time
		let pos = Vect.Clone(this.globalPosition);
		pos = Vect.Add(pos, Vect.MultiplyScalar(this._velocity, deltaTime));

		// applying the new position
		this.globalPosition = pos;
	}

	protected override CopyChildren(source: SceneEntity): void {
		
		let src = source as PhysicsEntity;
		let childs = source.entities;
		for(let i = 0; i < childs.length; i++){
			
			if(childs[i] == src._collider) continue;

			let childClone = childs[i].Clone();
			if(childClone != null) this.addChild(childClone);
		}
	}

	protected override CopyFields(source: SceneEntity): void {
		
		super.CopyFields(source);

		let src = source as PhysicsEntity;
		this._velocity = Vect.Clone(src._velocity);
		this.mass = src.mass;
	}
}