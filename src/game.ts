///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { AABBCollider, CircleCollider } from './collider';
import { GameScene } from './gameScene';
import { IVect, Vect } from './math';
import { Scene } from './scene';
import { SceneEntity } from './sceneEntity';

/**
 * data structure that handles the core game logic and loop
 */
export class Game{

	/**
	 * creates a game with the specified render resolution
	 * @param width the x resolution of the game renderer
	 * @param height the y resolution of the game renderer, equal to width if not specified
	 */
	public constructor(width: number = 0, height: number = width){
		this._renderer = new PIXI.Renderer({
			backgroundAlpha: 1,
			width: width,
			height: height
		});

		if(Game._instance != null){
			throw "Multiple singleton instances not allowed";
		}
		Game._instance = this;

		this._currentScene = GameScene.BasicScene(this);
		this._currentScene.OnEnter();

		this.RegisterType("SceneEntity", SceneEntity);
		this.RegisterType("GameScene", GameScene);
		this.RegisterType("AABBCollider", AABBCollider);
		this.RegisterType("CircleCollider", CircleCollider);
	}

	private static _instance: Game;
	public static get instance() {return this._instance; }

	private _registeredTypes: {typeName: string, type: any}[] = [];
	private _animFrameRequestID: number = -1;
	private _renderer: PIXI.Renderer;
	private _lastStepTimestamp: DOMHighResTimeStamp;
	private _frameRateCap = 11.11111; // 90 FPS
	private _currentScene: Scene;
	private _keysPressed: Array<boolean> = new Array(256);
	private _mousePos: IVect = Vect.Create();
	private _mouseButton: number = 0;
	private _mouseButtonsPressed: Array<boolean> = new Array(3);
	private _mousePressed: boolean = false;

	/**
	 * The pixijs renderer object used for rendering everything in the game
	 */
	public get renderer(){
		return this._renderer;
	}

	public get mousePos() { return this._mousePos; }
	public get mouseButton() { return this._mouseButton; }
	public get mousePressed() { return this._mousePressed; }

	public get maxFramesPerSecond(){
		return 1000 / this._frameRateCap;
	}
	public set maxFramesPerSecond(fps){
		this._frameRateCap = 1000 / fps;
	}

	public get currentScene(){ return this._currentScene; }
	public set currentScene(scene){

		this._currentScene.OnExit();
		this._currentScene = scene;
		this._currentScene.OnEnter();
	}

	public get keysPressed(): Array<boolean>{ return this._keysPressed; }
	public get mouseButtonsPressed(): Array<boolean>{ return this._mouseButtonsPressed; }

	/**
	 * add a type register the the type registry of the game
	 * @param typeName the name to add the type under
	 * @param type the class definition of the type
	 */
	public RegisterType(typeName: string, type: any): void{

		// check to see if any types are already registered under the specified name
		let register = this.GetTypeRegister(typeName);
		if(register != null){
			console.warn(
				"A type is already registered under the name '" + typeName + 
				"' in the game, the register will be set to reflect the new specified type '" + 
				type.toString() + "'"
			);
			register.type = type;
			return;
		}

		// check to see if the type is already registered under a different name
		let registeredName = this.GetRegisteredTypeName(type);
		if(registeredName != null){
			console.error("Type '" + type + "' is already registered in the game");
			return;
		}

		// add to the register if it's not already there
		let m = {typeName: typeName, type: type};
		this._registeredTypes.push(m);
	}

	/**
	 * look for a type in the registry under the specified name and return it
	 * @param typeName the name of the type to look for in the registry
	 */
	public GetRegisteredType(typeName: string): any{

		for(let i = this._registeredTypes.length - 1; i >= 0; i--){
			if(this._registeredTypes[i].typeName == typeName)
				return this._registeredTypes[i].type;
		}

		return null;
	}

	/**
	 * look for a name in the registry under the specified type and return it
	 * @param type the class definition to look for in the registry
	 */
	public GetRegisteredTypeName(type: any): string{

		for(let i = this._registeredTypes.length - 1; i >= 0; i--){
			if(this._registeredTypes[i].type == type)
				return this._registeredTypes[i].typeName;
		}

		return null;
	}

	private GetTypeRegister(typeName: string){
		
		for(let i = this._registeredTypes.length - 1; i >= 0; i--){
			if(this._registeredTypes[i].typeName == typeName)
				return this._registeredTypes[i];
		}
		
		return null;
	}

	/** starts listening to mouse events for the game */
	public AttachMouseEvents(): void{

		let ths = this;

		// NO RIGHT CLIK MENU >:C
		this.renderer.view.addEventListener('contextmenu', function(e){ 
			e.preventDefault(); 
			return false; 
		});

		this.renderer.view.addEventListener('mousedown', function(e){
			ths.OnMouseDown(e);
		});
		this.renderer.view.addEventListener('mouseup', function(e){
			ths.OnMouseUp(e);
		});
		this.renderer.view.addEventListener('mouseout', function(e){
			ths.OnMouseOut(e);
		});
		this.renderer.view.addEventListener('mousemove', function(e){
			ths.OnMouseMove(e);
		})
	}

	/**
	 * starts the game listening to keyboard events
	 */
	public AttachKeyboardEvents(): void{

		let ths = this;

		window.addEventListener('keydown', function(e){
			//console.log(e.key + ": " + e.keyCode);
			if(e.keyCode !== 73){
				e.preventDefault();
			}
			ths.OnKeyDown(e);
		});
		window.addEventListener('keyup', function(e){
			ths.OnKeyUp(e)
		});
	}

	/**
	 * stops listening for keyboard events
	 */
	public DetachKeyboardEvents(): void{

		// TODO
	}

	private OnKeyDown(e: KeyboardEvent){
		console.log(e.key + ": " + e.keyCode);
		this._keysPressed[e.keyCode] = true;
	}

	private OnKeyUp(e: KeyboardEvent){
		this._keysPressed[e.keyCode] = false;
	}

	private OnMouseDown(e: MouseEvent){

		// console.log(e.button);
		this._mouseButtonsPressed[e.button] = true;
		this._mouseButton = e.button;
		this._mousePressed = true;
		this._mousePos = {
			x: e.offsetX,
			y: e.offsetY
		};
	}

	private OnMouseMove(e: MouseEvent){
		this._mousePos = {
			x: e.offsetX,
			y: e.offsetY
		};
	}

	private OnMouseUp(e: MouseEvent){
		
		this._mouseButtonsPressed[e.button] = false;
		this._mousePressed = false;
		this._mousePos = {
			x: e.offsetX,
			y: e.offsetY
		};
	}

	private OnMouseOut(e: MouseEvent){
		
		for(let i = this._mouseButtonsPressed.length - 1; i >= 0; i--) {
			this._mouseButtonsPressed[i] = false;
		}

		this._mousePressed = false;
		this._mousePos = {
			x: e.offsetX,
			y: e.offsetY
		};
	}

	/**
	 * sets the game to start continuously performing update and draw cycles
	 */
	public StartGameLoop(): void{

		if(this._animFrameRequestID >= 0)
			return;

		this._lastStepTimestamp = performance.now();
		let ths = this;
		this._animFrameRequestID = requestAnimationFrame(function(time){
			ths.Step(time);
		});
	}

	/**
	 * stops the game from performing anymore update/draw cycles automatically
	 */
	public StopGameLoop(): void{
		cancelAnimationFrame(this._animFrameRequestID);
		this._animFrameRequestID = -1;
	}

	private Step(time: DOMHighResTimeStamp): void{

		let ths = this;
		this._animFrameRequestID = requestAnimationFrame(function(time){
			ths.Step(time);
		})

		let deltaTime = time - this._lastStepTimestamp;
		if(deltaTime <= this._frameRateCap)
			return;

		this._lastStepTimestamp = time;

		this.Update(deltaTime * 0.001);
		this.Draw();
	}

	/**
	 * Simulate gameplay logic for the specified amount of time
	 * @param deltaTime the ampunt of time in seconds that will be simulated in this update cycle
	 */
	public Update(deltaTime: number): void{

		this._currentScene?.Update(deltaTime);
	}

	/**
	 * Render the current game state to the render canvas
	 */
	public Draw(): void{
		
		// clear the render canvas to the default background color
		this._renderer.clear();

		this._currentScene?.Draw(this.renderer);
	}
}