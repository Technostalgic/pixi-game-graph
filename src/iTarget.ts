///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Collider } from './collider';
import { IVect } from './math';
import { SceneEntity } from './sceneEntity';

/** 
 * interface that should be implemented in every scene object that is able to be hit by an attack or
 * otherwise take damage 
 */
export interface ITarget{

	/**
	 * apply hit damage to the target
	 * @param damage the amount of damage to apply
	 * @param impulse the amount of force to apply to the target
	 * @param damager the entity that applied the damage
	 */
	Hit(damage: number, impulse: IVect, damager: SceneEntity): void;

	get collider(): Collider;
}

export namespace ITarget{

	/** Type checking discriminator for ITarget */
	export function implementedIn(obj: any): obj is ITarget{
		return(
			"Hit" in obj &&
			"collider" in obj
		);
	}
}