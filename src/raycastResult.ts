///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

import { Collider, CollisionLayer } from "./collider";
import { IVect } from "./math";
import { Ray } from "./ray";

export interface IRaycastResult{
	
	/** the ray that was cast */
	ray: Ray

	/** the collider that the raycast hit */
	collider: Collider

	/** if the ray hit the collider or not */
	didEnter: boolean;

	/** if the ray went all the way through the collider or not */
	didExit: boolean;

	/** the point of the ray that hits the collider */
	entryPoint: IVect;

	/** the point of the ray that exits the collider */
	exitPoint: IVect;

	/** the surface normal where the raycast hits the collider */
	entryNormal: IVect;

	/** the surface normal where the raycast exits the collider */
	exitNormal: IVect;

	/** how far through the colldier the ray goes*/
	penetration: number;
}

export interface ICirclecastResult extends IRaycastResult{
	
	/**
	 * the radius of the ray that was cast
	 */
	rayRadius: number;

	/**
	 * the point where the ray radius first hits the collider
	 */
	firstContactPoint: IVect;

	/**
	 * the point where the ray radius last overlaps the collider
	 */
	lastContactPoint: IVect;
}

export namespace RaycastResult{

	/**
	 * Create a raycast result from the specified data
	 * @param entered 
	 * @param exited 
	 * @param entryPoint 
	 * @param exitPoint 
	 * @param entryNorm 
	 * @param exitNorm 
	 * @param penetration 
	 */
	export function PrecalculatedRaycastResult(
		ray: Ray, collider: Collider,
		entered: boolean, exited: boolean, 
		entryPoint: IVect, exitPoint: IVect, 
		entryNorm: IVect, exitNorm: IVect, 
		penetration: number): 
	IRaycastResult{

		let r = {
			ray: ray,
			collider: collider,
			didEnter: entered,
			didExit: exited,
			entryPoint: entryPoint,
			exitPoint: exitPoint,
			entryNormal: entryNorm,
			exitNormal: exitNorm,
			penetration: penetration
		};
		return r;
	}

	/**
	 * Create a circlecast result from the specified data
	 * @param entered 
	 * @param exited 
	 * @param entryPoint 
	 * @param exitPoint 
	 * @param entryNorm 
	 * @param exitNorm 
	 * @param penetration 
	 */
	export function PrecalculatedCirclecastResult(
		ray: Ray, radius: number, collider: Collider,
		entered: boolean, exited: boolean, 
		entryPoint: IVect, exitPoint: IVect, 
		firstContact: IVect, lastContact: IVect,
		entryNorm: IVect, exitNorm: IVect, 
		penetration: number): 
	ICirclecastResult{

		let r = {
			ray: ray,
			rayRadius: radius,
			collider: collider,
			didEnter: entered,
			didExit: exited,
			entryPoint: entryPoint,
			exitPoint: exitPoint,
			firstContactPoint: firstContact,
			lastContactPoint: lastContact,
			entryNormal: entryNorm,
			exitNormal: exitNorm,
			penetration: penetration
		};
		return r;
	}
}