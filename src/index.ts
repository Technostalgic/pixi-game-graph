///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { Game } from './game';
import { GameScene } from './gameScene';

/**
 * entry point of the program
 */
function initialize(): void {

	document.body.style.textAlign = "center";
	document.body.style.backgroundColor = "#000";
	document.body.style.color = "#CCC";
	document.body.style.padding = "0px";
	document.body.style.margin = "0px";

	let game = new Game(800, 600);
	game.currentScene = GameScene.BasicScene(game);

	document.body.appendChild(game.renderer.view);
	game.renderer.backgroundColor = 0x202020;
	
	game.AttachKeyboardEvents();
	game.AttachMouseEvents();
	game.StartGameLoop();

	console.log("Debug game entry point:");
	console.log(this);
}

window.addEventListener('load', initialize);