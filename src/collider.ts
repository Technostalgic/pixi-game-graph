///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Collision, ICollision } from './iCollision';
import { GameEvent } from './gameEvent';
import { IActivate } from './iActivate';
import { ICollider } from './iCollider';
import { IRect, IVect, Rect, Side, Vect } from './math';
import { Ray } from './ray';
import { ICirclecastResult, IRaycastResult, RaycastResult } from './raycastResult';
import { Scene } from './scene';
import { SceneEntity } from './sceneEntity';

export enum CollisionLayer {
	none = 0,
	terrain = 1 << 0,
	actors = 1 << 1,
	props = 1 << 2,
	projectiles = 1 << 3,
	effects = 1 << 4
}

/**
 * A collider is an entity that is used for detecting collisions with other colliders, or raycasting
 * against
 */
export abstract class Collider extends SceneEntity implements IActivate, ICollider{

	public constructor(){
		super();
	}

	// ---------------------------------------------------------------------------------------------
	private _layer: CollisionLayer = CollisionLayer.none;
	protected _onCollision: GameEvent<ICollision> = new GameEvent<ICollision>();
	protected _wireframeGraphic: PIXI.Graphics = null;
	private _wireframeInfo: {color: number, width: number} = null;

	protected _isActivated: boolean = true;
	public get isActivated() { return this._isActivated; }

	public get layer() { return this._layer; }
	public set layer(lyr) { 
		if(this.parentScene != null)
			throw "not implemented";
		this._layer = lyr;
	}

	public get collider() { return this; }

	public get onCollision() { return this._onCollision; }

	// ---------------------------------------------------------------------------------------------
	/** @inheritdoc */
	protected override OnAddedToScene(scene: Scene): void{
		super.OnAddedToScene(scene);
		
		if(this.isActivated) this.OnActivated();
	}

	/** @inheritdoc */
	protected override OnRemovedFromScene(scene: Scene): void{

		if(this.isActivated) {

			// remove from collider list
			scene?.colliders.RemoveCollider(this);
		}

		super.OnRemovedFromScene(scene);
	}

	protected OnActivated(): void{

		// add to parent scene collider array
		if(this.parentScene != null)
			this.parentScene.colliders.AddCollider(this);
			
		this._isActivated = true;
	}

	protected OnDeactivated(): void{
		
		// remove from collider list
		this.parentScene?.colliders.RemoveCollider(this);

		this._isActivated = false;
	}

	/** @inheritdoc */
	public SetActivated(active: boolean): void{
		if(active) this.Activate();
		else this.Deactivate();
	}

	public Activate(): void{
		
		if(this._isActivated) return;
		this.OnActivated();
		this._isActivated = true;
	}

	public Deactivate(): void{

		if(!this._isActivated) return;
		this.OnDeactivated();
		this._isActivated = false;
	}

	// ---------------------------------------------------------------------------------------------
	public abstract GetColliderAABB(): IRect;

	/**
	 * tests to see if two colliders are overlapping
	 * @param collider the collider to test against
	 */
	public abstract OverlapsCollider(collider: Collider): Boolean;

	/**
	 * generates collision data for this and another overlapping collider
	 * @param collider the collider that is overlapping this collider
	 */
	public abstract GetCollision(collider: Collider): ICollision;
	
	/**
	 * tests to see if the specified point overlaps the collider
	 * @param point the point to test
	 */
	public abstract OverlapsPoint(point: IVect): Boolean;

	/**
	 * returns the closest point on the collider to the specified target
	 * @param point the target point
	 */
	public abstract ClosestPoint(point: IVect): IVect;

	public abstract RayCast(ray: Ray): IRaycastResult;

	public abstract CircleCast(Ray: Ray, radius: number): ICirclecastResult;

	// ---------------------------------------------------------------------------------------------
	/** @inheritdoc */
	public override destroy(options?: PIXI.IDestroyOptions | boolean): void{

		this.OnRemovedFromScene(this.parentScene);
		
		super.destroy(options);
	}

	// ---------------------------------------------------------------------------------------------
	/**
	 * creates an outline graphic of the collider's axis aligned bounding box, is not added to self
	 * @param color the color of the wireframe
	 * @param width the thickness of the wireframe
	 */
	public CreateAABBWireframe(color: number, width: number): 
	PIXI.Graphics{

		let r = new PIXI.Graphics();

		let bounds = this.GetColliderAABB();
		r.lineStyle({
			alpha: 1,
			width: width,
			color: color
		});

		r.drawRect(0, 0, bounds.size.x, bounds.size.y);

		return r;
	}

	/**
	 * creates an outline graphic of the collider and add it to itself
	 * @param color the color of the wireframe
	 * @param width the thickness of the wireframe
	 * @param addSelf whether or not the graphic object should be added as a child to the collider
	 */
	public abstract CreateWireframe(color: number, width: number): PIXI.Graphics;

	/**
	 * redraw the wireframe graphic
	 */
	public RedrawWireframe(){
		if(this._wireframeInfo != null){
			this._wireframeGraphic?.destroy();
			this._wireframeGraphic = null;
			this.CreateWireframe(this._wireframeInfo.color, this._wireframeInfo.width);
		}
	}

	/**
	 * record wireframe info when a wireframe graphic is created so that it can be serialized
	 * @param color the color that the wireframe will draw as
	 * @param width the line width of the wire frame graphic
	 */
	protected RecordWireFrameInfo(color: number, width: number){
		this._wireframeInfo = {color: color, width: width};
	}

	// ---------------------------------------------------------------------------------------------
	/** @inheritdoc */
	protected override CopyFields(source: SceneEntity): void {
		
		super.CopyFields(source);
		let src = source as Collider;

		if(src._wireframeGraphic != null){
			let wfg = src._wireframeGraphic.clone();
			wfg.position.set(
				src._wireframeGraphic.position.x,
				src._wireframeGraphic.position.y
			);
			this._wireframeGraphic = wfg;
			this.addChild(wfg);
		}

		// copy the wireframe information for serialization
		if(src._wireframeInfo != null){
			this._wireframeInfo = {
				color: src._wireframeInfo.color,
				width: src._wireframeInfo.width
			}
		}

		this._layer = src.layer;
		this._isActivated = src._isActivated;
	}

	/** @inheritdoc */
	protected OnPostDeserialize(): void {
		if(this._wireframeInfo != null){
			this.CreateWireframe(this._wireframeInfo.color, this._wireframeInfo.width);
		}
	}
}

/**
 * An Axis-Aligned Bounding Box Collider; this is a collider in the shape of a rectangle that cannot
 * be rotated. It has a center position, and a width and height, it's position is affected by it's 
 * parent nodes in the scene graph, but the size and rotation are not
 */
export class AABBCollider extends Collider{

	public constructor(){ super(); }

	/**
	 * Create an aabb collider from the specified parameters
	 * @param hextents the half extends of the collider
	 */
	static Create(hextents: IVect): AABBCollider{
		let r = new AABBCollider();

		r._halfExtents.x = hextents.x;
		r._halfExtents.y = hextents.y;

		return r;
	}

	// ---------------------------------------------------------------------------------------------
	private _halfExtents : IVect = Vect.Create();

	/** half the width (x), and half the height (y) of the box, in pixels */
	public get halfExtents() { return this._halfExtents; }
	public set halfExtents(vec) { this._halfExtents.x = vec.x; this._halfExtents.y = vec.y; }

	// ---------------------------------------------------------------------------------------------
	/**
	 * set the collider to match the specified rect
	 * @param rect 
	 */
	public SetRect(rect: IRect){
		this.globalPosition = Rect.Center(rect);
		this.halfExtents = Vect.MultiplyScalar(rect.size, 0.5);
	}

	/** @inheritdoc */
	public OverlapsCollider(collider: Collider): Boolean {
		
		if(collider instanceof AABBCollider){
			let selfBox = this.GetColliderAABB();
			let oBox = collider.GetColliderAABB();
			return Rect.Overlaps(selfBox, oBox);
		}
		else if(collider instanceof CircleCollider){
			return collider.OverlapsCollider(this);
		}

		throw "shape overlap detection not implemented";
	}

	/** @inheritdoc */
	public GetCollision(collider: Collider): ICollision {
		
		if(collider instanceof AABBCollider){
			return this.GetCollision_AABB(collider as AABBCollider);
		}
		else if(collider instanceof CircleCollider){
			return collider.GetCollision(this);
		}

		return null;
	}

	private GetCollision_AABB(collider: AABBCollider): ICollision {

		let selfBounds = this.GetColliderAABB();
		let overlapRect = Rect.OverlapRect(selfBounds, collider.GetColliderAABB());
		let overlapCenter = Rect.Center(overlapRect);
		
		// left side
		let sideDist = Math.abs(overlapCenter.x - selfBounds.position.x);
		let colSide = Side.left;

		// right side
		let rightSideDist = Math.abs(overlapCenter.x - (selfBounds.position.x + selfBounds.size.x));
		if(sideDist > rightSideDist){
			sideDist = rightSideDist;
			colSide = Side.right;
		}

		// top side
		let topSideDist = Math.abs(overlapCenter.y - selfBounds.position.y);
		if(sideDist > topSideDist){
			sideDist = topSideDist;
			colSide = Side.up;
		}

		// bottom side
		let bottomSideDist = Math.abs(overlapCenter.y - (selfBounds.position.y + selfBounds.size.y));
		if(sideDist > bottomSideDist){
			sideDist = bottomSideDist;
			colSide = Side.down;
		}

		// calculate collision normal based on which side of the collider is hit
		let colNormal = Vect.Create();
		switch(colSide){

			case Side.left:
				colNormal.x = 1;
				break;

			case Side.right:
				colNormal.x = -1;
				break;
			
			case Side.up:
				colNormal.y = 1;
				break;
				
			case Side.down:
				colNormal.y = -1;
				break;
		}

		return Collision.Create(this, collider, colNormal, overlapCenter, sideDist * 2);
	}

	/** @inheritdoc */
	public OverlapsPoint(point: IVect): Boolean {
		
		let gpos = this.globalPosition;
		let hext = this._halfExtents;

		return !(
			point.x < gpos.x - hext.x ||
			point.x > gpos.x + hext.x ||
			point.y < gpos.y - hext.y ||
			point.y > gpos.y + hext.y
		);
	}

	/** @inheritdoc */
	public ClosestPoint(point: IVect): IVect {
		
		let gpos = this.globalPosition;
		let hext = this._halfExtents;

		return {
			x: Math.min(Math.max(point.x, gpos.x - hext.x), gpos.x + hext.x),
			y: Math.min(Math.max(point.y, gpos.y - hext.y), gpos.y + hext.y)
		}
	}

	/** @inheritdoc */
	public GetColliderAABB(): IRect {

		let gpos = this.globalPosition;
		return Rect.Create(
			Vect.Create(gpos.x - this._halfExtents.x, gpos.y - this._halfExtents.y),
			Vect.Create(this._halfExtents.x * 2, this._halfExtents.y * 2)
		);
	}
	
	/** @inheritdoc */
	public RayCast(ray: Ray): IRaycastResult {
		
		// calculate raycast result and return it
		let r = Ray.CastAgainstRect(ray, this.GetColliderAABB());

		// apply collider reference to point to self
		if(r != null){
			r.collider = this;
		}

		return r;
	}

	/** @inheritdoc */
	public CircleCast(ray: Ray, radius: number): ICirclecastResult {
		// TODO Optomize all this shit
		
		// calculate the widest possible hit on the rect
		let rect = this.GetColliderAABB();
		let radius2 = radius * 2;
		rect.position.x -= radius;
		rect.position.y -= radius;
		rect.size.x += radius2;
		rect.size.y += radius2;
		let rectCastResult = Ray.CastAgainstRect(ray, rect);

		if(rectCastResult == null){
			return null;
		}

		let fcont = Vect.Add(
			rectCastResult.entryPoint, 
			Vect.MultiplyScalar(rectCastResult.entryNormal, -radius)
		);
		let lcont = Vect.Add(
			rectCastResult.exitPoint, 
			Vect.MultiplyScalar(rectCastResult.exitNormal, radius)
		);

		let r = RaycastResult.PrecalculatedCirclecastResult(
			ray, radius, this, rectCastResult.didEnter, rectCastResult.didExit,
			rectCastResult.entryPoint, rectCastResult.exitPoint, fcont, lcont, 
			rectCastResult.entryNormal, rectCastResult.exitNormal, rectCastResult.penetration
		);

		// if the entry point is a corner case
		let insetEntry = Vect.Add(
			rectCastResult.entryPoint, 
			Vect.MultiplyScalar(rectCastResult.entryNormal, radius * 0.5)
		);
		if(!this.OverlapsPoint(insetEntry)){
			
			// perform raycast against circle at nearest corner position with radius of circlecast
			let corner = this.ClosestPoint(insetEntry);
			let circCastResult = Ray.CastAgainstCircle(ray, corner, radius);

			// if the circle is hit, its the same as if a circlecast hit the corner of the box
			if(circCastResult != null && circCastResult.didEnter){
				r.didEnter = true;
				r.entryPoint = circCastResult.entryPoint;
				r.entryNormal = circCastResult.entryNormal;
			}
			else{
				return null;
			}
		}

		// if the exit point is a corner case
		let insetExit = Vect.Add(
			rectCastResult.exitPoint, 
			Vect.MultiplyScalar(rectCastResult.exitNormal, radius * 0.5)
		);
		if(!this.OverlapsPoint(insetExit)){
			
			// perform raycast against circle at nearest corner position with radius of circlecast
			let corner = this.ClosestPoint(insetExit);
			let circCastResult = Ray.CastAgainstCircle(ray, corner, radius);

			// if the circle is hit, its the same as if a circlecast hit the corner of the box
			if(circCastResult != null && circCastResult.didExit){
				r.didExit = true;
				r.exitPoint = circCastResult.exitPoint;
				r.exitNormal = circCastResult.exitNormal;
			}
			else{
				// return null;
			}
		}

		// return null if no entry or exit
		if(!r.didEnter && !r.didExit) {
			return null;
		}
		return r;
	}

	// ---------------------------------------------------------------------------------------------
	/** @inheritdoc */
	public override CreateWireframe(color: number, width: number){
		
		this.RecordWireFrameInfo(color, width);

		if(this._wireframeGraphic != null) { 
			this._wireframeGraphic.destroy();
		}
		this._wireframeGraphic = this.CreateAABBWireframe(color, width);
		this.addChild(this._wireframeGraphic);
		SceneEntity.SetGlobalPosition(
			this._wireframeGraphic, Vect.Subtract(this.globalPosition, this.halfExtents)
		);

		return this._wireframeGraphic;
	}

	// ---------------------------------------------------------------------------------------------
	protected override CopyFields(source: SceneEntity): void {
		
		super.CopyFields(source);

		let src = source as AABBCollider;
		this._halfExtents = Vect.Clone(src._halfExtents);
	}
}

/**
 * A Circle Collider; this is a collider in the shape of a circle, it has a center position and a
 * radius. It's position is affected by it's parent nodes in the scene graph but it's radius is not
 * affected by the scale
 */
export class CircleCollider extends Collider{

	public constructor(){ super(); }

	public static Create(radius: number): CircleCollider{
		let r = new CircleCollider();
		r._radius = radius;
		return r;
	}

	private _radius: number = 0;
	
	/** The radius of the circle, in pixels */
	public get radius() { return this._radius; }
	public set radius(rad) { this._radius = rad; }

	// ---------------------------------------------------------------------------------------------
	/** @inheritdoc */
	public GetColliderAABB(): IRect {
		
		let  pos = this.globalPosition;
		pos.x -= this._radius;
		pos.y -= this._radius;

		let r = Rect.Create(pos, Vect.Create(this._radius * 2));
		return r;
	}

	/** @inheritdoc */
	public OverlapsCollider(collider: Collider): Boolean {

		if(collider instanceof CircleCollider){
			let oCirc = collider as CircleCollider;
			
			let distSq = Vect.DistanceSquared(oCirc.globalPosition, this.globalPosition);
			let radSq = oCirc.radius + this.radius;
			radSq *= radSq;

			return distSq <= radSq;
		}
		
		let pos = this.globalPosition;
		return Vect.DistanceSquared(collider.ClosestPoint(pos), pos) <= (this.radius * this.radius);
	}
	
	/** @inheritdoc */
	public GetCollision(collider: Collider): ICollision {

		if(collider instanceof CircleCollider){
			return this.GetCollision_Circle(collider);
		}
		else if(collider instanceof AABBCollider){
			return this.GetCollision_AABB(collider);
		}

		throw new Error('Method not implemented.');
	}

	private GetCollision_Circle(other: CircleCollider): ICollision{

		let posA = this.globalPosition;
		let posB = other.globalPosition;
		let invDif = Vect.Subtract(posA, posB);
		let point = Vect.Create(
			(posA.x + posB.x) * 0.5,
			(posA.y + posB.y) * 0.5
		);

		return Collision.Create(
			this, 
			other, 
			Vect.Normalize(invDif), 
			point, 
			(this.radius + other.radius) - Vect.Magnitude(invDif)
		);
	}

	private GetCollision_AABB(other: AABBCollider): ICollision{

		let posA = this.globalPosition;
		let posB = other.ClosestPoint(posA);
		let dif = Vect.Subtract(posB, posA);
		let norm = Vect.Invert(Vect.Normalize(dif));
		let pen = this.radius - Vect.Magnitude(dif);
		let point = Vect.Add(posA, Vect.MultiplyScalar(norm, -this.radius - (pen * 0.5)));

		return Collision.Create(this, other, norm, point, pen);
	}

	/** @inheritdoc */
	public OverlapsPoint(point: IVect): Boolean {
		let pos = this.globalPosition;
		let dx = pos.x - point.x;
		let dy = pos.y - point.y;
		let distSq = dx * dx + dy * dy;
		let radSq = this._radius * this._radius;

		return distSq <= radSq;
	}

	/** @inheritdoc */
	public ClosestPoint(point: IVect): IVect {
		
		let pos = this.globalPosition;
		let dif = Vect.Subtract(point, pos);
		let mag = Math.min(Vect.Magnitude(dif), this.radius);
		dif = Vect.Normalize(dif);
		dif = Vect.MultiplyScalar(dif, mag);

		return Vect.Add(pos, dif);
	}

	/** @inheritdoc */
	public RayCast(ray: Ray): IRaycastResult {
		
		let r = Ray.CastAgainstCircle(ray, this.globalPosition, this._radius);

		if(r != null){
			r.collider = this;
		}

		return r;
	}

	/** @inheritdoc */
	public CircleCast(ray: Ray, radius: number): ICirclecastResult {
		
		let castResult = Ray.CastAgainstCircle(ray, this.globalPosition, this._radius + radius);

		// null if no intersection
		if(castResult == null) return null;

		let fc = Vect.Add(
			castResult.entryPoint,
			Vect.MultiplyScalar(castResult.entryNormal, -radius)
		);

		let lc = Vect.Add(
			castResult.exitPoint,
			Vect.MultiplyScalar(castResult.exitNormal, -radius)
		);

		// create circlecast result from raycast
		return RaycastResult.PrecalculatedCirclecastResult(
			ray, radius, this, castResult.didEnter, castResult.didExit, castResult.entryPoint, 
			castResult.exitPoint, fc, lc, castResult.exitNormal, castResult.exitNormal, 
			castResult.penetration
		);
	}

	// ---------------------------------------------------------------------------------------------
	/** @inheritdoc */
	public override CreateWireframe(color: number, width: number){
		this.RecordWireFrameInfo(color, width);
		let r = new PIXI.Graphics();

		if(this._wireframeGraphic != null) this._wireframeGraphic.destroy();
		this._wireframeGraphic = r;

		r.lineStyle({
			color: color,
			width: width
		});
		r.drawCircle(0, 0, this.radius);

		this.addChild(r);
		return r;
	}

	// ---------------------------------------------------------------------------------------------
	protected override CopyFields(source: SceneEntity): void {
		
		super.CopyFields(source);

		let src = source as CircleCollider;
		this.radius = src.radius;
	}
}