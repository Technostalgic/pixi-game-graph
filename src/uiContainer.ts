///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js'
import { AABBCollider, Collider } from "./collider";
import { Game } from "./game";
import { IRect, IVect, Rect, Vect } from "./math";
import { Scene } from "./scene";
import { UIElement } from "./uiElement";

export class UIContainer extends Scene{

	constructor(game: Game){ 
		super(game); 

		this._uiRect = Rect.Create(Vect.Create(), Vect.Create(
			game.renderer.view.width, 
			game.renderer.view.height
		));
	}
	
	private _uiRect: IRect;
	public get uiRect(){ return this._uiRect; }

	// ---------------------------------------------------------------------------------------------
	public DrawUI(renderer: PIXI.Renderer){
		renderer.render(this, {
			clear: false
		});
	}
	
	// ---------------------------------------------------------------------------------------------
	GetUIElementsAtPoint(point: IVect): Array<UIElement>{

		let col = new AABBCollider();
		col.SetRect(Rect.Create(point, Vect.Create(1, 1)))

		return this.GetUIElementsOnCollider(col);
	}

	GetUIElementsOnCollider(collider: Collider): Array<UIElement>{
		
		let r = new Array<UIElement>();
		let cols = this.colliders.FindOverlappingColliders(collider);

		for(let i = 0; i < cols.length; i++){
			if(cols[i].parentEntity instanceof UIElement){
				r.push(cols[i].parentEntity as UIElement);
			}
		}

		return r;
	}
}