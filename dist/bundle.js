/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 465:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CollisionLayer": () => (/* binding */ CollisionLayer),
/* harmony export */   "Collider": () => (/* binding */ Collider),
/* harmony export */   "AABBCollider": () => (/* binding */ AABBCollider),
/* harmony export */   "CircleCollider": () => (/* binding */ CircleCollider)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(49);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _iCollision__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(867);
/* harmony import */ var _gameEvent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(219);
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(564);
/* harmony import */ var _ray__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(422);
/* harmony import */ var _raycastResult__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(651);
/* harmony import */ var _sceneEntity__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(721);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///







var CollisionLayer;
(function (CollisionLayer) {
    CollisionLayer[CollisionLayer["none"] = 0] = "none";
    CollisionLayer[CollisionLayer["terrain"] = 1] = "terrain";
    CollisionLayer[CollisionLayer["actors"] = 2] = "actors";
    CollisionLayer[CollisionLayer["props"] = 4] = "props";
    CollisionLayer[CollisionLayer["projectiles"] = 8] = "projectiles";
    CollisionLayer[CollisionLayer["effects"] = 16] = "effects";
})(CollisionLayer || (CollisionLayer = {}));
/**
 * A collider is an entity that is used for detecting collisions with other colliders, or raycasting
 * against
 */
var Collider = /** @class */ (function (_super) {
    __extends(Collider, _super);
    function Collider() {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._layer = CollisionLayer.none;
        _this._onCollision = new _gameEvent__WEBPACK_IMPORTED_MODULE_2__.GameEvent();
        _this._wireframeGraphic = null;
        _this._wireframeInfo = null;
        _this._isActivated = true;
        return _this;
    }
    Object.defineProperty(Collider.prototype, "isActivated", {
        get: function () { return this._isActivated; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Collider.prototype, "layer", {
        get: function () { return this._layer; },
        set: function (lyr) {
            if (this.parentScene != null)
                throw "not implemented";
            this._layer = lyr;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Collider.prototype, "collider", {
        get: function () { return this; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Collider.prototype, "onCollision", {
        get: function () { return this._onCollision; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Collider.prototype.OnAddedToScene = function (scene) {
        _super.prototype.OnAddedToScene.call(this, scene);
        if (this.isActivated)
            this.OnActivated();
    };
    /** @inheritdoc */
    Collider.prototype.OnRemovedFromScene = function (scene) {
        if (this.isActivated) {
            // remove from collider list
            scene === null || scene === void 0 ? void 0 : scene.colliders.RemoveCollider(this);
        }
        _super.prototype.OnRemovedFromScene.call(this, scene);
    };
    Collider.prototype.OnActivated = function () {
        // add to parent scene collider array
        if (this.parentScene != null)
            this.parentScene.colliders.AddCollider(this);
        this._isActivated = true;
    };
    Collider.prototype.OnDeactivated = function () {
        var _a;
        // remove from collider list
        (_a = this.parentScene) === null || _a === void 0 ? void 0 : _a.colliders.RemoveCollider(this);
        this._isActivated = false;
    };
    /** @inheritdoc */
    Collider.prototype.SetActivated = function (active) {
        if (active)
            this.Activate();
        else
            this.Deactivate();
    };
    Collider.prototype.Activate = function () {
        if (this._isActivated)
            return;
        this.OnActivated();
        this._isActivated = true;
    };
    Collider.prototype.Deactivate = function () {
        if (!this._isActivated)
            return;
        this.OnDeactivated();
        this._isActivated = false;
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Collider.prototype.destroy = function (options) {
        this.OnRemovedFromScene(this.parentScene);
        _super.prototype.destroy.call(this, options);
    };
    // ---------------------------------------------------------------------------------------------
    /**
     * creates an outline graphic of the collider's axis aligned bounding box, is not added to self
     * @param color the color of the wireframe
     * @param width the thickness of the wireframe
     */
    Collider.prototype.CreateAABBWireframe = function (color, width) {
        var r = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        var bounds = this.GetColliderAABB();
        r.lineStyle({
            alpha: 1,
            width: width,
            color: color
        });
        r.drawRect(0, 0, bounds.size.x, bounds.size.y);
        return r;
    };
    /**
     * redraw the wireframe graphic
     */
    Collider.prototype.RedrawWireframe = function () {
        var _a;
        if (this._wireframeInfo != null) {
            (_a = this._wireframeGraphic) === null || _a === void 0 ? void 0 : _a.destroy();
            this._wireframeGraphic = null;
            this.CreateWireframe(this._wireframeInfo.color, this._wireframeInfo.width);
        }
    };
    /**
     * record wireframe info when a wireframe graphic is created so that it can be serialized
     * @param color the color that the wireframe will draw as
     * @param width the line width of the wire frame graphic
     */
    Collider.prototype.RecordWireFrameInfo = function (color, width) {
        this._wireframeInfo = { color: color, width: width };
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Collider.prototype.CopyFields = function (source) {
        _super.prototype.CopyFields.call(this, source);
        var src = source;
        if (src._wireframeGraphic != null) {
            var wfg = src._wireframeGraphic.clone();
            wfg.position.set(src._wireframeGraphic.position.x, src._wireframeGraphic.position.y);
            this._wireframeGraphic = wfg;
            this.addChild(wfg);
        }
        // copy the wireframe information for serialization
        if (src._wireframeInfo != null) {
            this._wireframeInfo = {
                color: src._wireframeInfo.color,
                width: src._wireframeInfo.width
            };
        }
        this._layer = src.layer;
        this._isActivated = src._isActivated;
    };
    /** @inheritdoc */
    Collider.prototype.OnPostDeserialize = function () {
        if (this._wireframeInfo != null) {
            this.CreateWireframe(this._wireframeInfo.color, this._wireframeInfo.width);
        }
    };
    return Collider;
}(_sceneEntity__WEBPACK_IMPORTED_MODULE_6__.SceneEntity));

/**
 * An Axis-Aligned Bounding Box Collider; this is a collider in the shape of a rectangle that cannot
 * be rotated. It has a center position, and a width and height, it's position is affected by it's
 * parent nodes in the scene graph, but the size and rotation are not
 */
var AABBCollider = /** @class */ (function (_super) {
    __extends(AABBCollider, _super);
    function AABBCollider() {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._halfExtents = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Create();
        return _this;
    }
    /**
     * Create an aabb collider from the specified parameters
     * @param hextents the half extends of the collider
     */
    AABBCollider.Create = function (hextents) {
        var r = new AABBCollider();
        r._halfExtents.x = hextents.x;
        r._halfExtents.y = hextents.y;
        return r;
    };
    Object.defineProperty(AABBCollider.prototype, "halfExtents", {
        /** half the width (x), and half the height (y) of the box, in pixels */
        get: function () { return this._halfExtents; },
        set: function (vec) { this._halfExtents.x = vec.x; this._halfExtents.y = vec.y; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    /**
     * set the collider to match the specified rect
     * @param rect
     */
    AABBCollider.prototype.SetRect = function (rect) {
        this.globalPosition = _math__WEBPACK_IMPORTED_MODULE_3__.Rect.Center(rect);
        this.halfExtents = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.MultiplyScalar(rect.size, 0.5);
    };
    /** @inheritdoc */
    AABBCollider.prototype.OverlapsCollider = function (collider) {
        if (collider instanceof AABBCollider) {
            var selfBox = this.GetColliderAABB();
            var oBox = collider.GetColliderAABB();
            return _math__WEBPACK_IMPORTED_MODULE_3__.Rect.Overlaps(selfBox, oBox);
        }
        else if (collider instanceof CircleCollider) {
            return collider.OverlapsCollider(this);
        }
        throw "shape overlap detection not implemented";
    };
    /** @inheritdoc */
    AABBCollider.prototype.GetCollision = function (collider) {
        if (collider instanceof AABBCollider) {
            return this.GetCollision_AABB(collider);
        }
        else if (collider instanceof CircleCollider) {
            return collider.GetCollision(this);
        }
        return null;
    };
    AABBCollider.prototype.GetCollision_AABB = function (collider) {
        var selfBounds = this.GetColliderAABB();
        var overlapRect = _math__WEBPACK_IMPORTED_MODULE_3__.Rect.OverlapRect(selfBounds, collider.GetColliderAABB());
        var overlapCenter = _math__WEBPACK_IMPORTED_MODULE_3__.Rect.Center(overlapRect);
        // left side
        var sideDist = Math.abs(overlapCenter.x - selfBounds.position.x);
        var colSide = _math__WEBPACK_IMPORTED_MODULE_3__.Side.left;
        // right side
        var rightSideDist = Math.abs(overlapCenter.x - (selfBounds.position.x + selfBounds.size.x));
        if (sideDist > rightSideDist) {
            sideDist = rightSideDist;
            colSide = _math__WEBPACK_IMPORTED_MODULE_3__.Side.right;
        }
        // top side
        var topSideDist = Math.abs(overlapCenter.y - selfBounds.position.y);
        if (sideDist > topSideDist) {
            sideDist = topSideDist;
            colSide = _math__WEBPACK_IMPORTED_MODULE_3__.Side.up;
        }
        // bottom side
        var bottomSideDist = Math.abs(overlapCenter.y - (selfBounds.position.y + selfBounds.size.y));
        if (sideDist > bottomSideDist) {
            sideDist = bottomSideDist;
            colSide = _math__WEBPACK_IMPORTED_MODULE_3__.Side.down;
        }
        // calculate collision normal based on which side of the collider is hit
        var colNormal = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Create();
        switch (colSide) {
            case _math__WEBPACK_IMPORTED_MODULE_3__.Side.left:
                colNormal.x = 1;
                break;
            case _math__WEBPACK_IMPORTED_MODULE_3__.Side.right:
                colNormal.x = -1;
                break;
            case _math__WEBPACK_IMPORTED_MODULE_3__.Side.up:
                colNormal.y = 1;
                break;
            case _math__WEBPACK_IMPORTED_MODULE_3__.Side.down:
                colNormal.y = -1;
                break;
        }
        return _iCollision__WEBPACK_IMPORTED_MODULE_1__.Collision.Create(this, collider, colNormal, overlapCenter, sideDist * 2);
    };
    /** @inheritdoc */
    AABBCollider.prototype.OverlapsPoint = function (point) {
        var gpos = this.globalPosition;
        var hext = this._halfExtents;
        return !(point.x < gpos.x - hext.x ||
            point.x > gpos.x + hext.x ||
            point.y < gpos.y - hext.y ||
            point.y > gpos.y + hext.y);
    };
    /** @inheritdoc */
    AABBCollider.prototype.ClosestPoint = function (point) {
        var gpos = this.globalPosition;
        var hext = this._halfExtents;
        return {
            x: Math.min(Math.max(point.x, gpos.x - hext.x), gpos.x + hext.x),
            y: Math.min(Math.max(point.y, gpos.y - hext.y), gpos.y + hext.y)
        };
    };
    /** @inheritdoc */
    AABBCollider.prototype.GetColliderAABB = function () {
        var gpos = this.globalPosition;
        return _math__WEBPACK_IMPORTED_MODULE_3__.Rect.Create(_math__WEBPACK_IMPORTED_MODULE_3__.Vect.Create(gpos.x - this._halfExtents.x, gpos.y - this._halfExtents.y), _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Create(this._halfExtents.x * 2, this._halfExtents.y * 2));
    };
    /** @inheritdoc */
    AABBCollider.prototype.RayCast = function (ray) {
        // calculate raycast result and return it
        var r = _ray__WEBPACK_IMPORTED_MODULE_4__.Ray.CastAgainstRect(ray, this.GetColliderAABB());
        // apply collider reference to point to self
        if (r != null) {
            r.collider = this;
        }
        return r;
    };
    /** @inheritdoc */
    AABBCollider.prototype.CircleCast = function (ray, radius) {
        // TODO Optomize all this shit
        // calculate the widest possible hit on the rect
        var rect = this.GetColliderAABB();
        var radius2 = radius * 2;
        rect.position.x -= radius;
        rect.position.y -= radius;
        rect.size.x += radius2;
        rect.size.y += radius2;
        var rectCastResult = _ray__WEBPACK_IMPORTED_MODULE_4__.Ray.CastAgainstRect(ray, rect);
        if (rectCastResult == null) {
            return null;
        }
        var fcont = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Add(rectCastResult.entryPoint, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.MultiplyScalar(rectCastResult.entryNormal, -radius));
        var lcont = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Add(rectCastResult.exitPoint, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.MultiplyScalar(rectCastResult.exitNormal, radius));
        var r = _raycastResult__WEBPACK_IMPORTED_MODULE_5__.RaycastResult.PrecalculatedCirclecastResult(ray, radius, this, rectCastResult.didEnter, rectCastResult.didExit, rectCastResult.entryPoint, rectCastResult.exitPoint, fcont, lcont, rectCastResult.entryNormal, rectCastResult.exitNormal, rectCastResult.penetration);
        // if the entry point is a corner case
        var insetEntry = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Add(rectCastResult.entryPoint, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.MultiplyScalar(rectCastResult.entryNormal, radius * 0.5));
        if (!this.OverlapsPoint(insetEntry)) {
            // perform raycast against circle at nearest corner position with radius of circlecast
            var corner = this.ClosestPoint(insetEntry);
            var circCastResult = _ray__WEBPACK_IMPORTED_MODULE_4__.Ray.CastAgainstCircle(ray, corner, radius);
            // if the circle is hit, its the same as if a circlecast hit the corner of the box
            if (circCastResult != null && circCastResult.didEnter) {
                r.didEnter = true;
                r.entryPoint = circCastResult.entryPoint;
                r.entryNormal = circCastResult.entryNormal;
            }
            else {
                return null;
            }
        }
        // if the exit point is a corner case
        var insetExit = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Add(rectCastResult.exitPoint, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.MultiplyScalar(rectCastResult.exitNormal, radius * 0.5));
        if (!this.OverlapsPoint(insetExit)) {
            // perform raycast against circle at nearest corner position with radius of circlecast
            var corner = this.ClosestPoint(insetExit);
            var circCastResult = _ray__WEBPACK_IMPORTED_MODULE_4__.Ray.CastAgainstCircle(ray, corner, radius);
            // if the circle is hit, its the same as if a circlecast hit the corner of the box
            if (circCastResult != null && circCastResult.didExit) {
                r.didExit = true;
                r.exitPoint = circCastResult.exitPoint;
                r.exitNormal = circCastResult.exitNormal;
            }
            else {
                // return null;
            }
        }
        // return null if no entry or exit
        if (!r.didEnter && !r.didExit) {
            return null;
        }
        return r;
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    AABBCollider.prototype.CreateWireframe = function (color, width) {
        this.RecordWireFrameInfo(color, width);
        if (this._wireframeGraphic != null) {
            this._wireframeGraphic.destroy();
        }
        this._wireframeGraphic = this.CreateAABBWireframe(color, width);
        this.addChild(this._wireframeGraphic);
        _sceneEntity__WEBPACK_IMPORTED_MODULE_6__.SceneEntity.SetGlobalPosition(this._wireframeGraphic, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Subtract(this.globalPosition, this.halfExtents));
        return this._wireframeGraphic;
    };
    // ---------------------------------------------------------------------------------------------
    AABBCollider.prototype.CopyFields = function (source) {
        _super.prototype.CopyFields.call(this, source);
        var src = source;
        this._halfExtents = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Clone(src._halfExtents);
    };
    return AABBCollider;
}(Collider));

/**
 * A Circle Collider; this is a collider in the shape of a circle, it has a center position and a
 * radius. It's position is affected by it's parent nodes in the scene graph but it's radius is not
 * affected by the scale
 */
var CircleCollider = /** @class */ (function (_super) {
    __extends(CircleCollider, _super);
    function CircleCollider() {
        var _this = _super.call(this) || this;
        _this._radius = 0;
        return _this;
    }
    CircleCollider.Create = function (radius) {
        var r = new CircleCollider();
        r._radius = radius;
        return r;
    };
    Object.defineProperty(CircleCollider.prototype, "radius", {
        /** The radius of the circle, in pixels */
        get: function () { return this._radius; },
        set: function (rad) { this._radius = rad; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    CircleCollider.prototype.GetColliderAABB = function () {
        var pos = this.globalPosition;
        pos.x -= this._radius;
        pos.y -= this._radius;
        var r = _math__WEBPACK_IMPORTED_MODULE_3__.Rect.Create(pos, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Create(this._radius * 2));
        return r;
    };
    /** @inheritdoc */
    CircleCollider.prototype.OverlapsCollider = function (collider) {
        if (collider instanceof CircleCollider) {
            var oCirc = collider;
            var distSq = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.DistanceSquared(oCirc.globalPosition, this.globalPosition);
            var radSq = oCirc.radius + this.radius;
            radSq *= radSq;
            return distSq <= radSq;
        }
        var pos = this.globalPosition;
        return _math__WEBPACK_IMPORTED_MODULE_3__.Vect.DistanceSquared(collider.ClosestPoint(pos), pos) <= (this.radius * this.radius);
    };
    /** @inheritdoc */
    CircleCollider.prototype.GetCollision = function (collider) {
        if (collider instanceof CircleCollider) {
            return this.GetCollision_Circle(collider);
        }
        else if (collider instanceof AABBCollider) {
            return this.GetCollision_AABB(collider);
        }
        throw new Error('Method not implemented.');
    };
    CircleCollider.prototype.GetCollision_Circle = function (other) {
        var posA = this.globalPosition;
        var posB = other.globalPosition;
        var invDif = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Subtract(posA, posB);
        var point = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Create((posA.x + posB.x) * 0.5, (posA.y + posB.y) * 0.5);
        return _iCollision__WEBPACK_IMPORTED_MODULE_1__.Collision.Create(this, other, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Normalize(invDif), point, (this.radius + other.radius) - _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Magnitude(invDif));
    };
    CircleCollider.prototype.GetCollision_AABB = function (other) {
        var posA = this.globalPosition;
        var posB = other.ClosestPoint(posA);
        var dif = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Subtract(posB, posA);
        var norm = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Invert(_math__WEBPACK_IMPORTED_MODULE_3__.Vect.Normalize(dif));
        var pen = this.radius - _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Magnitude(dif);
        var point = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Add(posA, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.MultiplyScalar(norm, -this.radius - (pen * 0.5)));
        return _iCollision__WEBPACK_IMPORTED_MODULE_1__.Collision.Create(this, other, norm, point, pen);
    };
    /** @inheritdoc */
    CircleCollider.prototype.OverlapsPoint = function (point) {
        var pos = this.globalPosition;
        var dx = pos.x - point.x;
        var dy = pos.y - point.y;
        var distSq = dx * dx + dy * dy;
        var radSq = this._radius * this._radius;
        return distSq <= radSq;
    };
    /** @inheritdoc */
    CircleCollider.prototype.ClosestPoint = function (point) {
        var pos = this.globalPosition;
        var dif = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Subtract(point, pos);
        var mag = Math.min(_math__WEBPACK_IMPORTED_MODULE_3__.Vect.Magnitude(dif), this.radius);
        dif = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Normalize(dif);
        dif = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.MultiplyScalar(dif, mag);
        return _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Add(pos, dif);
    };
    /** @inheritdoc */
    CircleCollider.prototype.RayCast = function (ray) {
        var r = _ray__WEBPACK_IMPORTED_MODULE_4__.Ray.CastAgainstCircle(ray, this.globalPosition, this._radius);
        if (r != null) {
            r.collider = this;
        }
        return r;
    };
    /** @inheritdoc */
    CircleCollider.prototype.CircleCast = function (ray, radius) {
        var castResult = _ray__WEBPACK_IMPORTED_MODULE_4__.Ray.CastAgainstCircle(ray, this.globalPosition, this._radius + radius);
        // null if no intersection
        if (castResult == null)
            return null;
        var fc = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Add(castResult.entryPoint, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.MultiplyScalar(castResult.entryNormal, -radius));
        var lc = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Add(castResult.exitPoint, _math__WEBPACK_IMPORTED_MODULE_3__.Vect.MultiplyScalar(castResult.exitNormal, -radius));
        // create circlecast result from raycast
        return _raycastResult__WEBPACK_IMPORTED_MODULE_5__.RaycastResult.PrecalculatedCirclecastResult(ray, radius, this, castResult.didEnter, castResult.didExit, castResult.entryPoint, castResult.exitPoint, fc, lc, castResult.exitNormal, castResult.exitNormal, castResult.penetration);
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    CircleCollider.prototype.CreateWireframe = function (color, width) {
        this.RecordWireFrameInfo(color, width);
        var r = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        if (this._wireframeGraphic != null)
            this._wireframeGraphic.destroy();
        this._wireframeGraphic = r;
        r.lineStyle({
            color: color,
            width: width
        });
        r.drawCircle(0, 0, this.radius);
        this.addChild(r);
        return r;
    };
    // ---------------------------------------------------------------------------------------------
    CircleCollider.prototype.CopyFields = function (source) {
        _super.prototype.CopyFields.call(this, source);
        var src = source;
        this.radius = src.radius;
    };
    return CircleCollider;
}(Collider));



/***/ }),

/***/ 633:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ColliderPartitions": () => (/* binding */ ColliderPartitions)
/* harmony export */ });
/* harmony import */ var _collider__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(465);

/**
 * meant to be an object that separates a set of colliders into spatial partitions, and then only
 * checks collisions between those who share a partition instead of checking each collider against
 * every other collider. This would be done for optomization, but I haven't actually implemented
 * it yet.
 */
var ColliderPartitions = /** @class */ (function () {
    function ColliderPartitions() {
        this.any = new Array();
        this.terrain = new Array();
        this.actors = new Array();
        this.props = new Array();
        this.projectiles = new Array();
        this.effects = new Array();
    }
    // ---------------------------------------------------------------------------------------------
    ColliderPartitions.prototype.GetCompatibleCollisionLayers = function (layer) {
        var r = new Array();
        // TODO finish implementation
        switch (layer) {
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.none:
                r.push(this.any, this.terrain, this.actors, this.props, this.projectiles, this.effects);
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors:
                r.push(this.terrain, this.actors, this.props, this.projectiles);
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.projectiles:
                r.push(this.terrain, this.actors, this.props);
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain:
                r.push(this.any, this.actors, this.projectiles, this.props);
                break;
        }
        return r;
    };
    ColliderPartitions.prototype.AddCollider = function (collider, layer) {
        if (layer === void 0) { layer = null; }
        if (layer != null) {
            collider.layer = layer;
        }
        var array = this.any;
        switch (collider.layer) {
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain:
                array = this.terrain;
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors:
                array = this.actors;
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.props:
                array = this.props;
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.projectiles:
                array = this.projectiles;
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.effects:
                array = this.effects;
                break;
        }
        array.push(collider);
    };
    ColliderPartitions.prototype.RemoveCollider = function (collider) {
        var array = this.any;
        switch (collider.layer) {
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain:
                array = this.terrain;
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors:
                array = this.actors;
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.props:
                array = this.props;
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.projectiles:
                array = this.projectiles;
                break;
            case _collider__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.effects:
                array = this.effects;
                break;
        }
        var index = array.indexOf(collider);
        if (index >= 0) {
            array.splice(index, 1);
        }
    };
    ColliderPartitions.prototype.ForceCollision = function (colliderA, colliderB) {
        if (this._forcedCols == null) {
            this._forcedCols = new Array();
        }
        this._forcedCols.push(colliderA, colliderB);
    };
    ColliderPartitions.prototype.ContainsForcedCollision = function (colA, colB) {
        // iterate through each forced collision pair
        if (this._forcedCols != null)
            for (var i = this._forcedCols.length - 1; i > 0; i -= 2) {
                if ((this._forcedCols[i - 1] == colA && this._forcedCols[i] == colB) ||
                    (this._forcedCols[i - 1] == colB && this._forcedCols[i] == colA))
                    return true;
            }
        return false;
    };
    ColliderPartitions.prototype.FindOverlappingColliders = function (col) {
        var r = new Array();
        var layers = this.GetCompatibleCollisionLayers(col.layer);
        for (var i0 = layers.length - 1; i0 >= 0; i0--) {
            for (var i1 = layers[i0].length - 1; i1 >= 0; i1--) {
                if (col.OverlapsCollider(layers[i0][i1])) {
                    r.push(layers[i0][i1]);
                }
            }
        }
        return r;
    };
    ColliderPartitions.prototype.FindCollisions = function () {
        var r = new Array();
        // iterate through each terrain collider
        for (var i0 = this.terrain.length - 1; i0 >= 0; i0--) {
            var colA = this.terrain[i0];
            // check collisions for terrain vs actors
            for (var i1 = this.actors.length - 1; i1 >= 0; i1--) {
                var colB = this.actors[i1];
                if (this.ContainsForcedCollision(colA, colB))
                    continue;
                if (colA.OverlapsCollider(colB))
                    r.push(colA.GetCollision(colB));
            }
            // check collisions for terrain vs projectiles
            for (var i1 = this.projectiles.length - 1; i1 >= 0; i1--) {
                var colB = this.projectiles[i1];
                if (this.ContainsForcedCollision(colA, colB))
                    continue;
                if (colA.OverlapsCollider(colB))
                    r.push(colA.GetCollision(colB));
            }
        }
        // iterat through all actor colliders
        for (var i0 = this.projectiles.length - 1; i0 >= 0; i0--) {
            var colA = this.projectiles[i0];
            // check collisions for projectiles vs actors
            for (var i1 = this.actors.length - 1; i1 >= 0; i1--) {
                var colB = this.actors[i1];
                if (this.ContainsForcedCollision(colA, colB))
                    continue;
                if (colA.OverlapsCollider(colB))
                    r.push(colA.GetCollision(colB));
            }
        }
        // iterate through each forced collision
        if (this._forcedCols != null) {
            for (var i = this._forcedCols.length - 1; i > 0; i -= 2) {
                var colA = this._forcedCols[i - 1];
                var colB = this._forcedCols[i];
                r.push(colA.GetCollision(colB));
            }
            // clear the forced collisions
            this._forcedCols.splice(0, this._forcedCols.length);
        }
        return r;
    };
    ColliderPartitions.prototype.Raycast = function (ray, layer) {
        var r = new Array();
        // iterate through each layer that the raycast can collide with
        var layers = this.GetCompatibleCollisionLayers(layer);
        for (var i0 = layers.length - 1; i0 >= 0; i0--) {
            // iterate through each collider layer
            var layer_1 = layers[i0];
            for (var i1 = layer_1.length - 1; i1 >= 0; i1--) {
                // calculate raycast and append to results if necessary
                var collider = layer_1[i1];
                var result = collider.RayCast(ray);
                if (result != null && (result.didEnter || result.didExit)) {
                    r.push(result);
                }
            }
        }
        return r;
    };
    ColliderPartitions.prototype.CircleCast = function (ray, radius, layer) {
        var r = new Array();
        // iterate through each layer that the raycast can collide with
        var layers = this.GetCompatibleCollisionLayers(layer);
        for (var i0 = layers.length - 1; i0 >= 0; i0--) {
            // iterate through each collider layer
            var layer_2 = layers[i0];
            for (var i1 = layer_2.length - 1; i1 >= 0; i1--) {
                // calculate circlecast and append to results if necessary
                var collider = layer_2[i1];
                var result = collider.CircleCast(ray, radius);
                if (result != null && (result.didEnter || result.didExit)) {
                    r.push(result);
                }
            }
        }
        return r;
    };
    return ColliderPartitions;
}());



/***/ }),

/***/ 218:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Game": () => (/* binding */ Game)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(49);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _collider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(465);
/* harmony import */ var _gameScene__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(913);
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(564);
/* harmony import */ var _sceneEntity__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(721);
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///





/**
 * data structure that handles the core game logic and loop
 */
var Game = /** @class */ (function () {
    /**
     * creates a game with the specified render resolution
     * @param width the x resolution of the game renderer
     * @param height the y resolution of the game renderer, equal to width if not specified
     */
    function Game(width, height) {
        if (width === void 0) { width = 0; }
        if (height === void 0) { height = width; }
        this._registeredTypes = [];
        this._animFrameRequestID = -1;
        this._frameRateCap = 11.11111; // 90 FPS
        this._keysPressed = new Array(256);
        this._mousePos = _math__WEBPACK_IMPORTED_MODULE_3__.Vect.Create();
        this._mouseButton = 0;
        this._mouseButtonsPressed = new Array(3);
        this._mousePressed = false;
        this._renderer = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Renderer({
            backgroundAlpha: 1,
            width: width,
            height: height
        });
        if (Game._instance != null) {
            throw "Multiple singleton instances not allowed";
        }
        Game._instance = this;
        this._currentScene = _gameScene__WEBPACK_IMPORTED_MODULE_2__.GameScene.BasicScene(this);
        this._currentScene.OnEnter();
        this.RegisterType("SceneEntity", _sceneEntity__WEBPACK_IMPORTED_MODULE_4__.SceneEntity);
        this.RegisterType("GameScene", _gameScene__WEBPACK_IMPORTED_MODULE_2__.GameScene);
        this.RegisterType("AABBCollider", _collider__WEBPACK_IMPORTED_MODULE_1__.AABBCollider);
        this.RegisterType("CircleCollider", _collider__WEBPACK_IMPORTED_MODULE_1__.CircleCollider);
    }
    Object.defineProperty(Game, "instance", {
        get: function () { return this._instance; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "renderer", {
        /**
         * The pixijs renderer object used for rendering everything in the game
         */
        get: function () {
            return this._renderer;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "mousePos", {
        get: function () { return this._mousePos; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "mouseButton", {
        get: function () { return this._mouseButton; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "mousePressed", {
        get: function () { return this._mousePressed; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "maxFramesPerSecond", {
        get: function () {
            return 1000 / this._frameRateCap;
        },
        set: function (fps) {
            this._frameRateCap = 1000 / fps;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "currentScene", {
        get: function () { return this._currentScene; },
        set: function (scene) {
            this._currentScene.OnExit();
            this._currentScene = scene;
            this._currentScene.OnEnter();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "keysPressed", {
        get: function () { return this._keysPressed; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "mouseButtonsPressed", {
        get: function () { return this._mouseButtonsPressed; },
        enumerable: false,
        configurable: true
    });
    /**
     * add a type register the the type registry of the game
     * @param typeName the name to add the type under
     * @param type the class definition of the type
     */
    Game.prototype.RegisterType = function (typeName, type) {
        // check to see if any types are already registered under the specified name
        var register = this.GetTypeRegister(typeName);
        if (register != null) {
            console.warn("A type is already registered under the name '" + typeName +
                "' in the game, the register will be set to reflect the new specified type '" +
                type.toString() + "'");
            register.type = type;
            return;
        }
        // check to see if the type is already registered under a different name
        var registeredName = this.GetRegisteredTypeName(type);
        if (registeredName != null) {
            console.error("Type '" + type + "' is already registered in the game");
            return;
        }
        // add to the register if it's not already there
        var m = { typeName: typeName, type: type };
        this._registeredTypes.push(m);
    };
    /**
     * look for a type in the registry under the specified name and return it
     * @param typeName the name of the type to look for in the registry
     */
    Game.prototype.GetRegisteredType = function (typeName) {
        for (var i = this._registeredTypes.length - 1; i >= 0; i--) {
            if (this._registeredTypes[i].typeName == typeName)
                return this._registeredTypes[i].type;
        }
        return null;
    };
    /**
     * look for a name in the registry under the specified type and return it
     * @param type the class definition to look for in the registry
     */
    Game.prototype.GetRegisteredTypeName = function (type) {
        for (var i = this._registeredTypes.length - 1; i >= 0; i--) {
            if (this._registeredTypes[i].type == type)
                return this._registeredTypes[i].typeName;
        }
        return null;
    };
    Game.prototype.GetTypeRegister = function (typeName) {
        for (var i = this._registeredTypes.length - 1; i >= 0; i--) {
            if (this._registeredTypes[i].typeName == typeName)
                return this._registeredTypes[i];
        }
        return null;
    };
    /** starts listening to mouse events for the game */
    Game.prototype.AttachMouseEvents = function () {
        var ths = this;
        // NO RIGHT CLIK MENU >:C
        this.renderer.view.addEventListener('contextmenu', function (e) {
            e.preventDefault();
            return false;
        });
        this.renderer.view.addEventListener('mousedown', function (e) {
            ths.OnMouseDown(e);
        });
        this.renderer.view.addEventListener('mouseup', function (e) {
            ths.OnMouseUp(e);
        });
        this.renderer.view.addEventListener('mouseout', function (e) {
            ths.OnMouseOut(e);
        });
        this.renderer.view.addEventListener('mousemove', function (e) {
            ths.OnMouseMove(e);
        });
    };
    /**
     * starts the game listening to keyboard events
     */
    Game.prototype.AttachKeyboardEvents = function () {
        var ths = this;
        window.addEventListener('keydown', function (e) {
            //console.log(e.key + ": " + e.keyCode);
            if (e.keyCode !== 73) {
                e.preventDefault();
            }
            ths.OnKeyDown(e);
        });
        window.addEventListener('keyup', function (e) {
            ths.OnKeyUp(e);
        });
    };
    /**
     * stops listening for keyboard events
     */
    Game.prototype.DetachKeyboardEvents = function () {
        // TODO
    };
    Game.prototype.OnKeyDown = function (e) {
        console.log(e.key + ": " + e.keyCode);
        this._keysPressed[e.keyCode] = true;
    };
    Game.prototype.OnKeyUp = function (e) {
        this._keysPressed[e.keyCode] = false;
    };
    Game.prototype.OnMouseDown = function (e) {
        // console.log(e.button);
        this._mouseButtonsPressed[e.button] = true;
        this._mouseButton = e.button;
        this._mousePressed = true;
        this._mousePos = {
            x: e.offsetX,
            y: e.offsetY
        };
    };
    Game.prototype.OnMouseMove = function (e) {
        this._mousePos = {
            x: e.offsetX,
            y: e.offsetY
        };
    };
    Game.prototype.OnMouseUp = function (e) {
        this._mouseButtonsPressed[e.button] = false;
        this._mousePressed = false;
        this._mousePos = {
            x: e.offsetX,
            y: e.offsetY
        };
    };
    Game.prototype.OnMouseOut = function (e) {
        for (var i = this._mouseButtonsPressed.length - 1; i >= 0; i--) {
            this._mouseButtonsPressed[i] = false;
        }
        this._mousePressed = false;
        this._mousePos = {
            x: e.offsetX,
            y: e.offsetY
        };
    };
    /**
     * sets the game to start continuously performing update and draw cycles
     */
    Game.prototype.StartGameLoop = function () {
        if (this._animFrameRequestID >= 0)
            return;
        this._lastStepTimestamp = performance.now();
        var ths = this;
        this._animFrameRequestID = requestAnimationFrame(function (time) {
            ths.Step(time);
        });
    };
    /**
     * stops the game from performing anymore update/draw cycles automatically
     */
    Game.prototype.StopGameLoop = function () {
        cancelAnimationFrame(this._animFrameRequestID);
        this._animFrameRequestID = -1;
    };
    Game.prototype.Step = function (time) {
        var ths = this;
        this._animFrameRequestID = requestAnimationFrame(function (time) {
            ths.Step(time);
        });
        var deltaTime = time - this._lastStepTimestamp;
        if (deltaTime <= this._frameRateCap)
            return;
        this._lastStepTimestamp = time;
        this.Update(deltaTime * 0.001);
        this.Draw();
    };
    /**
     * Simulate gameplay logic for the specified amount of time
     * @param deltaTime the ampunt of time in seconds that will be simulated in this update cycle
     */
    Game.prototype.Update = function (deltaTime) {
        var _a;
        (_a = this._currentScene) === null || _a === void 0 ? void 0 : _a.Update(deltaTime);
    };
    /**
     * Render the current game state to the render canvas
     */
    Game.prototype.Draw = function () {
        var _a;
        // clear the render canvas to the default background color
        this._renderer.clear();
        (_a = this._currentScene) === null || _a === void 0 ? void 0 : _a.Draw(this.renderer);
    };
    return Game;
}());



/***/ }),

/***/ 219:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GameEvent": () => (/* binding */ GameEvent)
/* harmony export */ });
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
/**
 * An event object that calls all it's listeners when the event is fired, the type parameter
 * specifies what type the listeners receive as data in their parameter when the function is called
 */
var GameEvent = /** @class */ (function () {
    function GameEvent() {
        /**
         * all the listeners that will be invoked when the event is fired
         */
        this.listeners = new Array();
    }
    /**
     * add a listener to be invoked when the event is invoked, listeners can not be added multiple
     * times
     * @param func the funciton to invoke when the event is fired
     */
    GameEvent.prototype.AddListener = function (func) {
        // ensure listener is not already added
        var index = this.listeners.indexOf(func);
        if (index >= 0)
            return;
        this.listeners.push(func);
    };
    /**
     * remove the specified listener from the event so it is no longer invoked
     * @param func
     */
    GameEvent.prototype.RemoveListener = function (func) {
        var index = this.listeners.indexOf(func);
        if (index >= 0)
            this.listeners.splice(index, 1);
    };
    /**
     * fire the event, invoke all listeners
     * @param data the event data to pass to the listener functions
     */
    GameEvent.prototype.Invoke = function (data) {
        for (var i = 0; i < this.listeners.length; i++) {
            this.listeners[i](data);
        }
    };
    return GameEvent;
}());



/***/ }),

/***/ 913:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GameScene": () => (/* binding */ GameScene)
/* harmony export */ });
/* harmony import */ var _scene__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(999);
/* harmony import */ var _collider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(465);
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(564);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



/**
 * A type of scene which is intended to be used for gameplay
 */
var GameScene = /** @class */ (function (_super) {
    __extends(GameScene, _super);
    function GameScene(game) {
        var _this = _super.call(this, game) || this;
        _this._name = "GameScene";
        return _this;
    }
    GameScene.BasicScene = function (game) {
        var r = new GameScene(game);
        r.GenerateBasicObjects();
        return r;
    };
    GameScene.prototype.GenerateBasicObjects = function () {
        var groundBottom = new _collider__WEBPACK_IMPORTED_MODULE_1__.AABBCollider();
        groundBottom.layer = _collider__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.terrain;
        groundBottom.halfExtents = _math__WEBPACK_IMPORTED_MODULE_2__.Vect.Create(150, 10);
        groundBottom.position.set(200, 250);
        groundBottom.CreateWireframe(0xFFFFFF, 1);
        var groundLeft = new _collider__WEBPACK_IMPORTED_MODULE_1__.AABBCollider();
        groundLeft.layer = _collider__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.terrain;
        groundLeft.halfExtents = _math__WEBPACK_IMPORTED_MODULE_2__.Vect.Create(10, 75);
        groundLeft.position.set(50, 175);
        groundLeft.CreateWireframe(0xFFFFFF, 1);
        var groundRight = new _collider__WEBPACK_IMPORTED_MODULE_1__.AABBCollider();
        groundRight.layer = _collider__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.terrain;
        groundRight.halfExtents = _math__WEBPACK_IMPORTED_MODULE_2__.Vect.Create(10, 75);
        groundRight.position.set(350, 175);
        groundRight.CreateWireframe(0xFFFFFF, 1);
        var groundTop = new _collider__WEBPACK_IMPORTED_MODULE_1__.AABBCollider();
        groundTop.layer = _collider__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.terrain;
        groundTop.halfExtents = _math__WEBPACK_IMPORTED_MODULE_2__.Vect.Create(150, 10);
        groundTop.position.set(200, 100);
        groundTop.CreateWireframe(0xFFFFFF, 1);
        var groundMid = new _collider__WEBPACK_IMPORTED_MODULE_1__.AABBCollider();
        groundMid.layer = _collider__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.terrain;
        groundMid.halfExtents = _math__WEBPACK_IMPORTED_MODULE_2__.Vect.Create(20, 20);
        groundMid.position.set(200, 175);
        groundMid.CreateWireframe(0xFFFFFF, 1);
        this.addChild(groundBottom, groundLeft, groundRight, groundTop, groundMid);
    };
    return GameScene;
}(_scene__WEBPACK_IMPORTED_MODULE_0__.Scene));



/***/ }),

/***/ 867:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Collision": () => (/* binding */ Collision)
/* harmony export */ });
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
var Collision;
(function (Collision) {
    /**
     * Create a collision with the specified data
     * @param a the primary collider involved in the collision
     * @param b the other collider involved in the collision
     * @param normal the normalized direciton vector that the 2-way collision force is directed in
     * @param point the centroid of the collision overlap area
     * @param penetration how much much the colliders are overlapping in the specified normal
     * 	direction
     */
    function Create(a, b, normal, point, penetration) {
        return {
            colliderA: a, colliderB: b,
            directionNormal: normal, collisionPoint: point,
            penetration: penetration
        };
    }
    Collision.Create = Create;
    /**
     * Recalculates a collision if either of the properties of the colliders have been modified
     * @param collision the collision to recalculate
     */
    function Recalculate(collision) {
        var r = collision.colliderA.GetCollision(collision.colliderB);
        collision.colliderA = r.colliderA;
        collision.colliderB = r.colliderB;
        collision.collisionPoint = r.collisionPoint;
        collision.directionNormal = r.directionNormal;
        collision.penetration = r.penetration;
    }
    Collision.Recalculate = Recalculate;
    /**
     * returns the collider involved in the collsision who is not the specified self, or null if the
     * specified self isn't involved in the collision at all
     * @param collision the collision to get the collider of
     * @param self the colldier that we do not want
     * @returns
     */
    function OtherCollider(collision, self) {
        if (collision.colliderA == self)
            return collision.colliderB;
        if (collision.colliderB == self)
            return collision.colliderA;
        return null;
    }
    Collision.OtherCollider = OtherCollider;
})(Collision || (Collision = {}));


/***/ }),

/***/ 564:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Side": () => (/* binding */ Side),
/* harmony export */   "Maths": () => (/* binding */ Maths),
/* harmony export */   "Vect": () => (/* binding */ Vect),
/* harmony export */   "Rect": () => (/* binding */ Rect)
/* harmony export */ });
var Side;
(function (Side) {
    Side[Side["none"] = 0] = "none";
    Side[Side["left"] = 1] = "left";
    Side[Side["right"] = 2] = "right";
    Side[Side["up"] = 4] = "up";
    Side[Side["top"] = 4] = "top";
    Side[Side["down"] = 8] = "down";
    Side[Side["bottom"] = 8] = "bottom";
    Side[Side["all"] = 15] = "all";
})(Side || (Side = {}));
(function (Side) {
    function RotateCW(side) {
        switch (side) {
            case Side.left: return Side.up;
            case Side.right: return Side.down;
            case Side.up: return Side.right;
            case Side.down: return Side.left;
        }
        return Side.none;
    }
    Side.RotateCW = RotateCW;
    function RotateCCW(side) {
        switch (side) {
            case Side.left: return Side.down;
            case Side.right: return Side.up;
            case Side.up: return Side.left;
            case Side.down: return Side.right;
        }
        return Side.none;
    }
    Side.RotateCCW = RotateCCW;
    function Opposite(side) {
        switch (side) {
            case Side.left: return Side.right;
            case Side.right: return Side.left;
            case Side.up: return Side.down;
            case Side.down: return Side.up;
        }
        return Side.none;
    }
    Side.Opposite = Opposite;
    function FromDirection(direction) {
        if (direction.x == 0 && direction.y == 0)
            return Side.none;
        if (Math.abs(direction.x) > Math.abs(direction.y)) {
            return direction.x < 0 ? Side.left : Side.right;
        }
        else {
            return direction.y < 0 ? Side.up : Side.down;
        }
    }
    Side.FromDirection = FromDirection;
    function GetAngle(side) {
        switch (side) {
            case Side.left: return Math.PI;
            case Side.right: return 0;
            case Side.up: return -Math.PI / 2;
            case Side.down: return Math.PI / 2;
        }
    }
    Side.GetAngle = GetAngle;
    function GetVector(side) {
        switch (side) {
            case Side.left: return Vect.Create(-1, 0);
            case Side.right: return Vect.Create(1, 0);
            case Side.up: return Vect.Create(0, -1);
            case Side.down: return Vect.Create(0, 1);
        }
        return Vect.Create(0);
    }
    Side.GetVector = GetVector;
})(Side || (Side = {}));
var Maths;
(function (Maths) {
    /**
     * calculates the smallest signed distance between two angles in radians
     * @param target the target angle to subtract from the source
     * @param source the source angle to be subtracted from
     */
    function SignedAngleDif(target, source) {
        var twoPi = Math.PI * 2;
        var pi = Math.PI;
        var r = target - source;
        r = ((r + pi) % twoPi + twoPi) % twoPi;
        r -= pi;
        return r;
    }
    Maths.SignedAngleDif = SignedAngleDif;
})(Maths || (Maths = {}));
var Vect;
(function (Vect) {
    /**
     * create a vector with the specified components
     * @param x the x component of the vector
     * @param y the y component of the vector
     */
    function Create(x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = x; }
        return {
            x: x,
            y: y
        };
    }
    Vect.Create = Create;
    function Clone(vect) {
        return {
            x: vect.x,
            y: vect.y
        };
    }
    Vect.Clone = Clone;
    function FromDirection(direction, magnitude) {
        if (magnitude === void 0) { magnitude = 1; }
        return Create(Math.cos(direction) * magnitude, Math.sin(direction) * magnitude);
    }
    Vect.FromDirection = FromDirection;
    /**
     * interpolate from point a to b, returns the interpolated value
     * @param from the vector to interpolate from, vector A
     * @param to the vector to interpolate to, vector B
     * @param delta how much interpolation from A to B has elapsed; 0 for none, 1 for 100%
     */
    function Lerp(from, to, delta) {
        var invDelta = 1 - delta;
        return {
            x: from.x * invDelta + to.x * delta,
            y: from.y * invDelta + to.y * delta
        };
    }
    Vect.Lerp = Lerp;
    /**
     * calculate the euclidean distance between two vectors
     * @param a the first vector
     * @param b the second vector
     */
    function Distance(a, b) {
        var dx = a.x - b.x;
        var dy = a.y - b.y;
        return Math.sqrt(dx * dx + dy * dy);
    }
    Vect.Distance = Distance;
    /**
     * returns the distance between two vectors, squared (more performant than calculating distance)
     * @param a the first vector
     * @param b the second vector
     * @returns
     */
    function DistanceSquared(a, b) {
        var dx = a.x - b.x;
        var dy = a.y - b.y;
        return dx * dx + dy * dy;
    }
    Vect.DistanceSquared = DistanceSquared;
    /**
     * Add together two vectors
     */
    function Add(a, b) {
        return {
            x: a.x + b.x,
            y: a.y + b.y
        };
    }
    Vect.Add = Add;
    /**
     * subtract b from a
     */
    function Subtract(a, b) {
        return {
            x: a.x - b.x,
            y: a.y - b.y
        };
    }
    Vect.Subtract = Subtract;
    /**
     * multiply each component of a with each component of b
     */
    function Multiply(a, b) {
        return {
            x: a.x * b.x,
            y: a.y * b.y
        };
    }
    Vect.Multiply = Multiply;
    /**
     * scale multuply each component a vector by a scalar
     */
    function MultiplyScalar(a, scalar) {
        return {
            x: a.x * scalar,
            y: a.y * scalar
        };
    }
    Vect.MultiplyScalar = MultiplyScalar;
    function Magnitude(a) {
        return Math.sqrt(a.x * a.x + a.y * a.y);
    }
    Vect.Magnitude = Magnitude;
    function Equal(a, b, leniency) {
        if (leniency === void 0) { leniency = 0.001; }
        return (Math.abs(a.x - b.x) <= leniency &&
            Math.abs(a.y - b.y) <= leniency);
    }
    Vect.Equal = Equal;
    /**
     * Calculate the dot product between two vectors
     * @param a the first vector
     * @param b the second vector
     */
    function Dot(a, b) {
        return a.x * b.x + a.y * b.y;
    }
    Vect.Dot = Dot;
    /**
     * calculate the projection of the specified vector onto another vector
     * @param a the vector to project
     * @param target the vector that a will be projected onto
     */
    function Project(a, target) {
        var dot = Dot(a, target);
        var quotient = Dot(target, target);
        var factor = dot / quotient;
        var r = MultiplyScalar(target, factor);
        return r;
    }
    Vect.Project = Project;
    function Direction(a) {
        return Math.atan2(a.y, a.x);
    }
    Vect.Direction = Direction;
    function Normalize(a) {
        var mag = Magnitude(a);
        if (mag <= 0)
            return { x: 0, y: 0 };
        return MultiplyScalar(a, 1 / mag);
    }
    Vect.Normalize = Normalize;
    function Rotate(a, rotation) {
        var mag = Magnitude(a);
        var dir = Direction(a);
        dir += rotation;
        return FromDirection(dir, mag);
    }
    Vect.Rotate = Rotate;
    function Invert(a) {
        return { x: -a.x, y: -a.y };
    }
    Vect.Invert = Invert;
})(Vect || (Vect = {}));
var Rect;
(function (Rect) {
    function Create(position, size) {
        if (position === void 0) { position = Vect.Create(); }
        if (size === void 0) { size = Vect.Create(); }
        return {
            position: position,
            size: size
        };
    }
    Rect.Create = Create;
    function Clone(rect) {
        return {
            position: Vect.Clone(rect.position),
            size: Vect.Clone(rect.size)
        };
    }
    Rect.Clone = Clone;
    function Center(rect) {
        return {
            x: rect.position.x + rect.size.x * 0.5,
            y: rect.position.y + rect.size.y * 0.5
        };
    }
    Rect.Center = Center;
    function Overlaps(a, b) {
        return !(a.position.x > b.position.x + b.size.x ||
            a.position.x + a.size.x < b.position.x ||
            a.position.y > b.position.y + b.size.y ||
            a.position.y + a.size.y < b.position.y);
    }
    Rect.Overlaps = Overlaps;
    /**
     * expands the rect a to fully overlap rect b, returns the modified rect a
     * @param a the rect to expand (this rect is modified)
     * @param b the rect that this rect should overlap
     */
    function ExpandToOverlap(a, b) {
        var minX = Math.min(a.position.x, b.position.x);
        var minY = Math.min(a.position.y, b.position.y);
        var maxX = Math.max(a.position.x + a.size.x, b.position.x + b.size.x);
        var maxY = Math.max(a.position.y + b.size.y, b.position.y + b.size.y);
        if (minX < a.position.x) {
            var dif = a.position.x - minX;
            a.position.x -= dif;
            a.size.x += dif;
        }
        if (minY < a.position.y) {
            var dif = a.position.y - minY;
            a.position.y -= dif;
            a.size.y += dif;
        }
        if (maxX > a.position.x + a.size.x) {
            a.size.x += maxX - (a.position.x + a.size.x);
        }
        if (maxY > a.position.y + a.size.y) {
            a.size.y += maxY - (a.position.y + a.size.y);
        }
        return a;
    }
    Rect.ExpandToOverlap = ExpandToOverlap;
    /** Return a rect that overlaps each specified rect */
    function OverlapRect(a, b) {
        var minX = a.position.x + a.size.x;
        var minY = a.position.y + a.size.y;
        var maxX = a.position.x;
        var maxY = a.position.y;
        minX = Math.min(minX, b.position.x + b.size.x);
        minY = Math.min(minY, b.position.y + b.size.y);
        maxX = Math.max(maxX, b.position.x);
        maxY = Math.max(maxY, b.position.y);
        return Rect.Create(Vect.Create(minX, minY), Vect.Create(maxX - minX, maxY - minY));
    }
    Rect.OverlapRect = OverlapRect;
    /**
     * Returns the side of the rect that is closest to the specified point
     * @param a the rect to check the sides of
     * @param b the point to compare the sides to
     */
    function ClosestSide(rect, point) {
        var aspect = rect.size.x / rect.size.y;
        var dif = Vect.Subtract(point, Rect.Center(rect));
        dif.x /= aspect;
        if (Math.abs(dif.x) > Math.abs(dif.y)) {
            return dif.x > 0 ? Side.right : Side.left;
        }
        return dif.y > 0 ? Side.down : Side.up;
    }
    Rect.ClosestSide = ClosestSide;
})(Rect || (Rect = {}));


/***/ }),

/***/ 422:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Ray": () => (/* binding */ Ray)
/* harmony export */ });
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(564);
/* harmony import */ var _raycastResult__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(651);
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


/**
 * a structure that is used for calculating raycasts againsy colliders, it is more efficient to use
 * and reuse the same ray structure (as lont as none of it's prperties change) because it caches
 * certain values that don't necessarily need to be recalculated if testing against different
 * collision geometry from the same ray.
 */
var Ray = /** @class */ (function () {
    function Ray() {
        this._rayStart = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
        this._rayEnd = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
    }
    Ray.CastAgainstRect = function (ray, rect) {
        var rectMin = rect.position;
        var rectMax = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(rectMin, rect.size);
        var rayOrigin = ray.rayStart;
        var dirFrac = ray.directionFrac;
        var t1 = (rectMin.x - rayOrigin.x) * dirFrac.x; // left side
        var t2 = (rectMax.x - rayOrigin.x) * dirFrac.x; // right side
        var t3 = (rectMin.y - rayOrigin.y) * dirFrac.y; // top side
        var t4 = (rectMax.y - rayOrigin.y) * dirFrac.y; // bottom side
        // the distance from entry point to ray origin
        var tmin = Math.max(Math.min(t1, t2), Math.min(t3, t4));
        // no hit
        if (tmin > ray.magnitude || tmin < 0) {
            return null;
        }
        // the distance from exit point to ray origin
        var tmax = Math.min(Math.max(t1, t2), Math.max(t3, t4));
        // no hit
        if (tmin > tmax) {
            return null;
        }
        var entryPoint = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(rayOrigin, _math__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(ray.directionNormal, tmin));
        var exitPoint = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(rayOrigin, _math__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(ray.directionNormal, tmax));
        var didEnter = tmin >= 0 && tmin <= ray.magnitude;
        var didExit = tmax >= 0 && tmax <= ray.magnitude;
        // calculate the penetration based on which parts of the ray entered and exited the collider
        var penetration = 0;
        if (!didExit && !didEnter) {
            penetration = ray.magnitude;
        }
        else if (!didExit) {
            penetration = ray.magnitude - tmin;
        }
        else if (!didEnter) {
            penetration = tmax;
        }
        else {
            penetration = tmax - tmin;
        }
        // calculate normals by seeing which side of the AABB collider the ray hit
        var entryNorm = null;
        switch (tmin) {
            case t1:
                entryNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(-1, 0);
                break;
            case t2:
                entryNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(1, 0);
                break;
            case t3:
                entryNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, -1);
                break;
            case t4:
                entryNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, 1);
                break;
        }
        var exitNorm = null;
        switch (tmax) {
            case t1:
                exitNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(-1, 0);
                break;
            case t2:
                exitNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(1, 0);
                break;
            case t3:
                exitNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, -1);
                break;
            case t4:
                exitNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, 1);
                break;
        }
        // construct raycast result object and return it
        var r = _raycastResult__WEBPACK_IMPORTED_MODULE_1__.RaycastResult.PrecalculatedRaycastResult(ray, null, didEnter, didExit, entryPoint, exitPoint, entryNorm, exitNorm, penetration);
        return r;
    };
    // TODO test this function
    Ray.CastAgainstCircle = function (ray, circlePos, circleRadius) {
        // offset the line to be relative to the circle
        var rayStart = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(ray.rayStart, circlePos);
        var rayEnd = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(ray.rayEnd, circlePos);
        // calculate the dot products between the origin and target vectors
        var aoa = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Dot(rayStart, rayStart);
        var aob = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Dot(rayStart, rayEnd);
        var bob = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Dot(rayEnd, rayEnd);
        // calculate the ratios used to find the determinant and the t-values
        var qa = aoa - 2.0 * aob + bob;
        var qb = -2.0 * aoa + 2.0 * aob;
        var qc = aoa - circleRadius * circleRadius;
        // caclulate the determinant used to determine if there was an intersection or not
        var determinant = qb * qb - 4.0 * qa * qc;
        // if the determinant is negative, the ray completely misses
        if (determinant < 0) {
            return null;
        }
        // compute the nearest of the two t values
        var tmin = (-qb - Math.sqrt(determinant)) / (2.0 * qa);
        // compute the furthest of the t values
        var tmax = (-qb + Math.sqrt(determinant)) / (2.0 * qa);
        // calculate raycast results from tmin and tmax
        var entered = tmin >= 0 && tmin <= 1;
        var exited = tmax >= 0 && tmin <= 1;
        var entryPoint = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Lerp(ray.rayStart, ray.rayEnd, tmin);
        var exitPoint = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Lerp(ray.rayStart, ray.rayEnd, tmax);
        var entryNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Normalize(_math__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(entryPoint, circlePos));
        var exitNorm = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Normalize(_math__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(exitPoint, circlePos));
        // calculate penetration based on where and whether the ray enters and exits the circle
        var penetration = 0;
        if (!entered && !exited) {
            penetration = ray.magnitude;
        }
        else if (!exited) {
            penetration = ray.magnitude - (tmin * ray.magnitude);
        }
        else if (!entered) {
            penetration = tmax * ray.magnitude;
        }
        else {
            penetration = (tmax - tmax) * ray.magnitude;
        }
        // create and return the raycast result
        var r = _raycastResult__WEBPACK_IMPORTED_MODULE_1__.RaycastResult.PrecalculatedRaycastResult(ray, null, entered, exited, entryPoint, exitPoint, entryNorm, exitNorm, penetration);
        return r;
    };
    /**
     * Create a ray object from the specified data
     * @param start the starting point of the ray
     * @param directionNormal the normal direction that the ray is facing (MUST be normalized)
     * @param length the length of the ray from start to end
     */
    Ray.Create = function (start, directionNormal, length) {
        var r = new Ray();
        r._rayStart = start;
        r._rayEnd = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(start, _math__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(directionNormal, length));
        r._directionNormal = directionNormal;
        r._magnitude = length;
        return r;
    };
    /**
     * Create a ray object from the specified start and end points
     * @param start the start of the ray
     * @param end the end of the ray
     */
    Ray.FromPoints = function (start, end) {
        var r = new Ray();
        r._rayStart = start;
        r._rayEnd = end;
        return r;
    };
    Object.defineProperty(Ray.prototype, "rayStart", {
        get: function () { return this._rayStart; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "rayEnd", {
        get: function () { return this._rayEnd; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "difference", {
        /** the vector from the start of the ray to the end of the ray */
        get: function () { var _a; return (_a = this._difference) !== null && _a !== void 0 ? _a : (this._difference = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(this._rayEnd, this._rayStart)); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "magnitude", {
        /** the length of the ray from start to end */
        get: function () { var _a; return (_a = this._magnitude) !== null && _a !== void 0 ? _a : (this._magnitude = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Magnitude(this.difference)); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "directionNormal", {
        /** the normalized vector that points in the same direction as the ray */
        get: function () { var _a; return (_a = this._directionNormal) !== null && _a !== void 0 ? _a : (this._directionNormal = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Normalize(this.difference)); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "directionFrac", {
        /** the multplicative inverse of the direction normal */
        get: function () {
            var _a;
            return (_a = this._directionFrac) !== null && _a !== void 0 ? _a : (this._directionFrac = _math__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(1 / this.directionNormal.x, 1 / this.directionNormal.y));
        },
        enumerable: false,
        configurable: true
    });
    return Ray;
}());



/***/ }),

/***/ 651:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RaycastResult": () => (/* binding */ RaycastResult)
/* harmony export */ });
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
var RaycastResult;
(function (RaycastResult) {
    /**
     * Create a raycast result from the specified data
     * @param entered
     * @param exited
     * @param entryPoint
     * @param exitPoint
     * @param entryNorm
     * @param exitNorm
     * @param penetration
     */
    function PrecalculatedRaycastResult(ray, collider, entered, exited, entryPoint, exitPoint, entryNorm, exitNorm, penetration) {
        var r = {
            ray: ray,
            collider: collider,
            didEnter: entered,
            didExit: exited,
            entryPoint: entryPoint,
            exitPoint: exitPoint,
            entryNormal: entryNorm,
            exitNormal: exitNorm,
            penetration: penetration
        };
        return r;
    }
    RaycastResult.PrecalculatedRaycastResult = PrecalculatedRaycastResult;
    /**
     * Create a circlecast result from the specified data
     * @param entered
     * @param exited
     * @param entryPoint
     * @param exitPoint
     * @param entryNorm
     * @param exitNorm
     * @param penetration
     */
    function PrecalculatedCirclecastResult(ray, radius, collider, entered, exited, entryPoint, exitPoint, firstContact, lastContact, entryNorm, exitNorm, penetration) {
        var r = {
            ray: ray,
            rayRadius: radius,
            collider: collider,
            didEnter: entered,
            didExit: exited,
            entryPoint: entryPoint,
            exitPoint: exitPoint,
            firstContactPoint: firstContact,
            lastContactPoint: lastContact,
            entryNormal: entryNorm,
            exitNormal: exitNorm,
            penetration: penetration
        };
        return r;
    }
    RaycastResult.PrecalculatedCirclecastResult = PrecalculatedCirclecastResult;
})(RaycastResult || (RaycastResult = {}));


/***/ }),

/***/ 999:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Scene": () => (/* binding */ Scene)
/* harmony export */ });
/* harmony import */ var _sceneEntity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(721);
/* harmony import */ var _colliderPartitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(633);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


/**
 * data structure that contains game entities
 */
var Scene = /** @class */ (function (_super) {
    __extends(Scene, _super);
    function Scene(game) {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._entitiesToRemove = new Array();
        _this._colliders = null;
        _this._parentGame = null;
        if (game !== undefined) {
            _this._parentGame = game;
        }
        _this._parentScene = _this;
        _this._name = "Scene";
        _this._colliders = new _colliderPartitions__WEBPACK_IMPORTED_MODULE_1__.ColliderPartitions();
        return _this;
    }
    Object.defineProperty(Scene.prototype, "parentScene", {
        get: function () { return this; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "parentGame", {
        get: function () { return this._parentGame; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "entitiesToRemove", {
        get: function () { return this._entitiesToRemove; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "colliders", {
        get: function () { return this._colliders; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    /**
     * should be called when scene is switched to
     */
    Scene.prototype.OnEnter = function () { };
    /**
     * should be called when scene is switched away from
     */
    Scene.prototype.OnExit = function () { };
    // ---------------------------------------------------------------------------------------------
    /**
     * Check collisions between all the colliders in the scene
     */
    Scene.prototype.CheckCollisions = function () {
        var collisions = this._colliders.FindCollisions();
        for (var i = collisions.length - 1; i >= 0; i--) {
            collisions[i].colliderA.onCollision.Invoke(collisions[i]);
            collisions[i].colliderB.onCollision.Invoke(collisions[i]);
        }
    };
    Scene.prototype.RemoveFlaggedEntities = function () {
        // iterate through each entity flagged for removal and remove them all
        for (var i = this._entitiesToRemove.length - 1; i >= 0; i--) {
            this._entitiesToRemove[i].destroy();
        }
        // remove them from the list
        this._entitiesToRemove.splice(0, this._entitiesToRemove.length);
    };
    /**
     * override to implement update functionality for the scene
     * @param deltaTime
     */
    Scene.prototype.Update = function (deltaTime) {
        // update all direct children
        _super.prototype.Update.call(this, deltaTime);
        // check collisions
        this.CheckCollisions();
        // remove entities that are to be removed
        this.RemoveFlaggedEntities();
    };
    /**
     * override to implement rendering functionality for the scene
     */
    Scene.prototype.Draw = function (renderer) {
        // render the scene graph
        this.scale.set(2);
        renderer.render(this);
        this.scale.set(1);
    };
    // ---------------------------------------------------------------------------------------------
    Scene.prototype.Clone = function () {
        var r = new (Object.getPrototypeOf(this).constructor)(this.parentGame);
        r.CopyChildren(this);
        r.CopyFields(this);
        return r;
    };
    /** @inheritdoc */
    Scene.prototype.GetSerializedObject = function () {
        // we don't want to serialize colldiers or parent game reference
        var o = [this._colliders, this._parentGame];
        this._colliders = undefined;
        var r = _super.prototype.GetSerializedObject.call(this);
        this._colliders = o[0];
        return r;
    };
    return Scene;
}(_sceneEntity__WEBPACK_IMPORTED_MODULE_0__.SceneEntity));



/***/ }),

/***/ 721:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SceneEntity": () => (/* binding */ SceneEntity)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(49);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _gameEvent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(219);
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(564);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///



var SceneEntity = /** @class */ (function (_super) {
    __extends(SceneEntity, _super);
    function SceneEntity() {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._removed = false;
        _this._name = null;
        _this._parentScene = null;
        _this._parentEntity = null;
        _this._entities = new Array();
        return _this;
    }
    Object.defineProperty(SceneEntity.prototype, "name", {
        get: function () {
            if (this._name != null)
                return this._name;
            if (this.parent != null && this.parent instanceof SceneEntity)
                return this.parent.name + "_chld";
            return "Entity";
        },
        set: function (nm) {
            this._name = nm;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "removed", {
        get: function () { return this._removed; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "parentGame", {
        get: function () { return this._parentScene.parentGame; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "parentScene", {
        get: function () { return this._parentScene; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "parentEntity", {
        get: function () {
            var _a;
            return (_a = this._parentEntity) !== null && _a !== void 0 ? _a : (this._parentEntity = this.parent);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "entities", {
        get: function () { return this._entities; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "globalPosition", {
        get: function () { return this.toGlobal(_math__WEBPACK_IMPORTED_MODULE_2__.Vect.Create()); },
        set: function (pos) {
            var _a, _b;
            var localPos = (_b = (_a = this.parent) === null || _a === void 0 ? void 0 : _a.toLocal(pos)) !== null && _b !== void 0 ? _b : _math__WEBPACK_IMPORTED_MODULE_2__.Vect.Clone(pos);
            this.position.set(localPos.x, localPos.y);
        },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    SceneEntity.GetGlobalPosition = function (obj) {
        return obj.toGlobal(_math__WEBPACK_IMPORTED_MODULE_2__.Vect.Create());
    };
    SceneEntity.SetGlobalPosition = function (obj, pos) {
        var localPos = obj.parent.toLocal(pos);
        obj.position.set(localPos.x, localPos.y);
    };
    ///---------------------------------------------------------------------------------------------
    /// Hierarchy
    /** called when this entity is added to another scene entity as a child */
    SceneEntity.prototype.OnAddedToParent = function (parent) {
        this._parentEntity = parent;
    };
    /** called when added to a new scene */
    SceneEntity.prototype.OnAddedToScene = function (scene) {
        this._parentScene = scene;
    };
    /** called when removed from a scene */
    SceneEntity.prototype.OnRemovedFromScene = function (scene) { };
    /**
     * for all child entities, ensure that they are also removed from the scene if this entity is
     * removed from a scene
     * @param scene
     */
    SceneEntity.prototype.UpdateChildrenRemovedFromScene = function (scene) {
        // iterate through each child entity
        for (var i = 0; i < this.entities.length; i++) {
            var ent = this.entities[i];
            ent.OnRemovedFromScene(scene);
            ent.UpdateChildrenRemovedFromScene(scene);
        }
    };
    /**
     * for all the entities that are children of this object, ensure that they share the
     * same parent scene as this object
     */
    SceneEntity.prototype.UpdateChildrenParentRef = function () {
        for (var i = this._entities.length - 1; i >= 0; i--) {
            // update entity new parent scene
            var ent = this._entities[i];
            ent.OnAddedToScene(this.parentScene);
            ent.OnAddedToParent(this);
            ent.UpdateChildrenParentRef();
        }
    };
    /** @inheritdoc */
    SceneEntity.prototype.addChild = function () {
        var childs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            childs[_i] = arguments[_i];
        }
        // base functionality
        var t = _super.prototype.addChild.apply(this, childs);
        // iterate through each child provided
        for (var i = 0; i < childs.length; i++) {
            // if it is not a scene entity, only perform base class addChild functionality
            if (!(childs[i] instanceof SceneEntity))
                continue;
            // otherwise, provide scene entity data
            var child = childs[i];
            // parent the child entity to our scene, and all of it's children too
            child.OnAddedToScene(this.parentScene);
            child.OnAddedToParent(this);
            child.UpdateChildrenParentRef();
            // add to child entities
            this._entities.push(child);
        }
        return t;
    };
    /** @inheritdoc */
    SceneEntity.prototype.removeChild = function () {
        var childs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            childs[_i] = arguments[_i];
        }
        // iterate through each child provided
        for (var i = 0; i < childs.length; i++) {
            // if it is not a scene entity, only perform base class removeChild functionality
            if (!(childs[0] instanceof SceneEntity))
                continue;
            var child = childs[0];
            var oscn = child.parentScene;
            child._parentEntity = null;
            child._parentScene = null;
            if (oscn != null) {
                child.OnRemovedFromScene(oscn);
                child.UpdateChildrenRemovedFromScene(oscn);
            }
            // remove from child entities
            var index = this._entities.indexOf(child);
            this._entities.splice(index, 1);
        }
        // base functionality
        return _super.prototype.removeChild.apply(this, childs);
    };
    /** @inheritdoc */
    SceneEntity.prototype.destroy = function (options) {
        var pent = this.parentEntity;
        this._parentEntity = null;
        _super.prototype.destroy.call(this, options);
        if (pent == null)
            return;
        var pind = pent._entities.indexOf(this);
        if (pind >= 0)
            pent._entities.splice(pind, 1);
        this._parentScene = null;
    };
    /**
     * Removes the entity from the scene at the end of the update cycle (before being rendered)
     */
    SceneEntity.prototype.RemoveFromScene = function () {
        this.parentScene.entitiesToRemove.push(this);
        this._removed = true;
    };
    /**
     * returns the first child entity of the specified type
     * @param type the type (constructor function) value to look for, must be the same type passed
     * 	in as the generic parameter
     */
    SceneEntity.prototype.GetChildOfType = function (type) {
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i] instanceof type) {
                return this.entities[i];
            }
        }
        return null;
    };
    ///---------------------------------------------------------------------------------------------
    /// Scene Methods
    SceneEntity.prototype.Update = function (deltaTime) {
        // iterate through each scene entity child in the entity container
        for (var i = 0; i < this._entities.length; i++) {
            // update the scene entity's game logic
            this._entities[i].Update(deltaTime);
        }
    };
    SceneEntity.prototype.Draw = function (renderer) { };
    ///---------------------------------------------------------------------------------------------
    /// Cloning
    SceneEntity.prototype.CopyFields = function (source) {
        var pos = source.position;
        this.position.set(pos.x, pos.y);
        this._name = source._name;
        this._parentEntity = source._parentEntity;
    };
    SceneEntity.prototype.CopyChildren = function (source) {
        var childs = source.entities;
        for (var i = 0; i < childs.length; i++) {
            var childClone = childs[i].Clone();
            if (childClone != null)
                this.addChild(childClone);
        }
    };
    /** creates a clone of the object and returns it */
    SceneEntity.prototype.Clone = function () {
        var r = new (Object.getPrototypeOf(this).constructor)();
        r.CopyChildren(this);
        r.CopyFields(this);
        return r;
    };
    ///---------------------------------------------------------------------------------------------
    /// Serialization
    /** returns a JSON serializable object that has all the necessary data to recreate itself */
    SceneEntity.prototype.GetSerializedObject = function () {
        // get the serialized type name from the game's type registry
        var serializedTypeName = this.parentGame.GetRegisteredTypeName(Object.getPrototypeOf(this).constructor);
        if (serializedTypeName == null) {
            console.error(this);
            throw ("The type for '" +
                Object.getPrototypeOf(this).constructor.name +
                "' has not been registered in the game");
        }
        // create an anonymous object to store all our fields into
        var r = {};
        r[SceneEntity.SFIELD_TYPE] = serializedTypeName;
        // create a field for, serialize and store all the children of this scene entity
        r[SceneEntity.SFIELD_CHILDREN] = [];
        for (var i = 0; i < this.entities.length; i++) {
            r[SceneEntity.SFIELD_CHILDREN].push(this.entities[i].GetSerializedObject());
        }
        // store the object position and name
        r._name = this._name;
        r.position = {
            x: this.position.x,
            y: this.position.y
        };
        // serialize and store the rest of the object fields
        this.SerializeFieldsInto(r);
        return r;
    };
    /**
     * copy all the fields of the entity into an anonymous object for serialization
     * @param obj the object to copy our fields into
     */
    SceneEntity.prototype.SerializeFieldsInto = function (obj, omittedTypes) {
        if (omittedTypes === void 0) { omittedTypes = []; }
        var dif = new SceneEntity();
        var untypedSelf = this;
        omittedTypes.push.apply(omittedTypes, [
            Function,
            pixi_js__WEBPACK_IMPORTED_MODULE_0__.DisplayObject,
            _gameEvent__WEBPACK_IMPORTED_MODULE_1__.GameEvent,
        ]);
        // iterate though each field we have and copy the necesary ones over to our 
        // serialized object
        fieldCopy: for (var field in untypedSelf) {
            // we don't want to copy any of these fields or
            // any field if it's shared with the base entity type (so we don't copy over a
            // bunch of useless redundant garbage into the serialized object)
            if (untypedSelf[field] === undefined ||
                field === "_parentGame" ||
                field === "_parentScene" ||
                dif[field] !== undefined) {
                continue fieldCopy;
            }
            // dont copy omitted types
            for (var i = omittedTypes.length - 1; i >= 0; i--) {
                if (untypedSelf[field] instanceof omittedTypes[i]) {
                    continue fieldCopy;
                }
            }
            // if it's a scene entity field, it must be a child reference
            if (untypedSelf[field] instanceof SceneEntity) {
                // if the field references an entity outside this object's children, don't store it
                var crIndex = this.entities.indexOf(untypedSelf[field]);
                if (crIndex < 0)
                    continue fieldCopy;
                // serialize a field to the reference into the object
                var childRef = {};
                childRef.type = SceneEntity.TYPE_CHILDREF;
                childRef[SceneEntity.SFIELD_CHILDINDEX] = crIndex;
                obj[field] = childRef;
                continue fieldCopy;
            }
            obj[field] = untypedSelf[field];
        }
    };
    /**
     * copy the field values from a serialized object
     * @param obj the serialized object to parse from
     */
    SceneEntity.prototype.CopyFieldsFromSerialized = function (obj, gameRef) {
        // cast self to anonymous object
        var untypedSelf = this;
        // iterate through each of the object's fields
        for (var fieldName in obj) {
            // don't copy the children
            if (fieldName == SceneEntity.SFIELD_CHILDREN) {
                continue;
            }
            // copy the field from the serialized object if we have a field of the same name
            if (untypedSelf[fieldName] !== undefined) {
                untypedSelf[fieldName] = SceneEntity.GetDeserializedField(obj, obj[fieldName], gameRef);
            }
        }
    };
    /**
     * copy the children into this object from a serialized object
     * @param obj the serialized object whose child list to parse and copy
     */
    SceneEntity.prototype.CopyChildrenFromSerialized = function (obj, gameRef) {
        if (obj[SceneEntity.SFIELD_CHILDREN] === undefined)
            return;
        for (var i = 0; i < obj[SceneEntity.SFIELD_CHILDREN].length; i++) {
            var child = SceneEntity.GetDeserializedObject(obj[SceneEntity.SFIELD_CHILDREN][i], gameRef);
            this.entities.push(child);
        }
    };
    /** called immediately after the object is deserialized */
    SceneEntity.prototype.OnPostDeserialize = function () { };
    /**
     * Deserialize an object based on it's 'type' property in the scope of the specified game
     * @param obj the object to deserialized
     * @param gameRef the reference to the game who's type registry will be used
     */
    SceneEntity.GetDeserializedObject = function (obj, gameRef) {
        if (obj[SceneEntity.SFIELD_TYPE] === undefined)
            return null;
        if (obj[SceneEntity.SFIELD_TYPE] === this.TYPE_CHILDREF) {
            throw ("Objects cannot be parsed as children without a reference to their sibling list," +
                "use SceneEntity.GetDeserializedField(...) instead");
        }
        // search for the serialized type
        var type = gameRef.GetRegisteredType(obj[SceneEntity.SFIELD_TYPE]);
        var ur = new type();
        if (ur._parentGame === null) {
            ur._parentGame = gameRef;
        }
        var r = ur;
        // copy/deserialize the children and object fields
        r.CopyChildrenFromSerialized(obj, gameRef);
        r.CopyFieldsFromSerialized(obj, gameRef);
        // truncate children that are null (probably did not parse properly)
        for (var i = r[SceneEntity.SFIELD_CHILDREN].length - 1; i >= 0; i--) {
            if (r[SceneEntity.SFIELD_CHILDREN][i] == null) {
                r[SceneEntity.SFIELD_CHILDREN].splice(i, 1);
            }
        }
        r === null || r === void 0 ? void 0 : r.OnPostDeserialize();
        return r;
    };
    /**
     * Similar to GetDeserializedObject, with different rules for what is returned if the data type
     * isn't a serialized SceneEntity. In this case we'll want to return the value itself if the
     * object doesn't appear to be a serialized scen entity. This is because the value is probably a
     * number, string, array or other literal type that we want to handle as a value type
     * @param fieldObj the reference to the object to deserialize
     * @param gameRef the reference to the game who's type registry will be used
     */
    SceneEntity.GetDeserializedField = function (fieldOwner, fieldObj, gameRef) {
        if (fieldObj == null)
            return null;
        // parse the object as a reference to one of the object's children
        if (fieldObj[SceneEntity.SFIELD_TYPE] === this.TYPE_CHILDREF) {
            var child = fieldOwner.childs[fieldObj[this.SFIELD_CHILDINDEX]];
            return child;
        }
        // parse the
        var obj = this.GetDeserializedObject(fieldObj, gameRef);
        if (obj == null)
            return fieldObj;
        return fieldObj;
    };
    SceneEntity.TYPE_CHILDREF = "_childRef";
    SceneEntity.SFIELD_TYPE = "type";
    SceneEntity.SFIELD_CHILDREN = "_entities";
    SceneEntity.SFIELD_CHILDINDEX = "childIndex";
    return SceneEntity;
}(pixi_js__WEBPACK_IMPORTED_MODULE_0__.Container));



/***/ }),

/***/ 49:
/***/ ((module) => {

module.exports = window["PIXI"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _game__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(218);
/* harmony import */ var _gameScene__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(913);
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


/**
 * entry point of the program
 */
function initialize() {
    document.body.style.textAlign = "center";
    document.body.style.backgroundColor = "#000";
    document.body.style.color = "#CCC";
    document.body.style.padding = "0px";
    document.body.style.margin = "0px";
    var game = new _game__WEBPACK_IMPORTED_MODULE_0__.Game(800, 600);
    game.currentScene = _gameScene__WEBPACK_IMPORTED_MODULE_1__.GameScene.BasicScene(game);
    document.body.appendChild(game.renderer.view);
    game.renderer.backgroundColor = 0x202020;
    game.AttachKeyboardEvents();
    game.AttachMouseEvents();
    game.StartGameLoop();
    console.log("Debug game entry point:");
    console.log(this);
}
window.addEventListener('load', initialize);

})();

window.Kinesis = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=bundle.js.map